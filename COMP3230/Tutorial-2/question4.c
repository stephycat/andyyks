#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>


int main(int argc, char *argv[]){

  pid_t pid, child1, child2;
  int status =0;


  if ((child1 = fork())==0) { //parent process
    int child1_status = 0;
    if ((child2 = fork()) ==0) {
      do {
        waitpid(child1, &child1_status,0);
      } while (!(WIFEXITED(child1_status)));
      sleep(1);
      execl("/bin/date", "date", "+%d-%m-%y", NULL);
      perror("execl() failure!\n");
      printf("This print is after execl() and should not have been executed if execl were successful! \n");
      _exit(1);
    }
    execlp("bin/ls", "ls", "-l", (char*)0);
    printf("The call to execlp() was not successful.\n");
    exit(127);
  } else { //parents
    wait(&status); // block
    sleep(2);
    printf("The END of program!\n");
  }
  return 0;
} //main
