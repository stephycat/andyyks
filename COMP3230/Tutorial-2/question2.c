#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#define CHILD_PROCESS(N) fprintf(stdout, "Child Process %d\n", N); exit(0)

int main(int argc, char *argv[]) {
  fprintf(stdout, "The START of Program!\n");

  if (fork() == 0) {
    sleep(2);
    CHILD_PROCESS(3);
  }

  if (fork() == 0) {
    CHILD_PROCESS(1);
  }

  if (fork() == 0) {
    sleep(2);
    CHILD_PROCESS(4);
  }

  if (fork() == 0) {
    CHILD_PROCESS(2);
  }

  int i;
  for (i=1; i<=4; i++, wait(NULL));
  fprintf(stdout, "The END of Program!\n");

  return EXIT_SUCCESS;
}
