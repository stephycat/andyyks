#!/bin/bash

clear

g++ -std=c++11 client$1.cpp Matrix.cpp -o client$1.out
./client$1.out < input$1.txt

rm -f client$1.out
