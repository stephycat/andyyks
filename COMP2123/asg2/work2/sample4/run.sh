#!/bin/bash

g++ -std=c++11 sample4.cpp Matrix.cpp Equation_solving.cpp -o sample4.out
./sample4.out < input.in
rm -f sample4.out
