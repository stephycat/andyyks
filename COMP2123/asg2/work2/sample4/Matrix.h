// The code needs you to finish it

#ifndef MATRIX_H
#define MATRIX_H

#include <iostream>

class Matrix
{
  private:
    int rows;
    int cols;
    float ***data;
    float ***data_original;
    float ***data_transposed;
    bool is_transposed;

  public:
    Matrix();  
    Matrix(int r, int c);  
    Matrix(const Matrix& m);
    Matrix(Matrix&& m);  
    Matrix& operator= (const Matrix& m);

    ~Matrix();  

    // Member functions like
    int get_row_dimension() const; // the number of rows;
    int get_column_dimension() const; // the number of columns;
    float get_entry(int r, int c) const;
    void row_switching(int i, int j);
    void row_multiplication(int i, float k);
    void row_addition(int i, int j, float k);
    void transpose();
    
    void transpose_prime();

    // Overloaded arithmetic operators
    friend std::istream & operator>> (std::istream& cin, Matrix& m);
    friend std::ostream & operator<< (std::ostream& cout, const Matrix& m);
    friend Matrix operator+ (const Matrix& m1, const Matrix& m2);
    friend Matrix operator* (const float scalar, const Matrix& m);
    friend Matrix operator* (const Matrix& m, const float scalar);
    friend Matrix operator* (const Matrix& m1, const Matrix& m2);

};

#endif
