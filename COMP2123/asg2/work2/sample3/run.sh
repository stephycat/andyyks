#!/bin/bash

g++ -std=c++11 sample3.cpp Matrix.cpp Equation_solving.cpp -o sample3.out
./sample3.out < input.in
rm -f sample3.out
