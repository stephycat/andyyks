#include <iostream>
#include <string>
#include "Matrix.h"

using namespace std;

Matrix::Matrix()
{
}

Matrix::Matrix(int r, int c):Matrix()
{
  if (r <= 0 or c <= 0)
    cout << "Invalid number of rows or columns!" << endl;
  data = new float *[r];
  for (int i = 0; i < r; i++)
    data[i] = new float[c];
  for (int i = 0; i < r; i++)
    for (int j = 0; j < c; j++)
      data[i][j] = 0;
  this->column = c; this->row = r;
}

Matrix::Matrix(const Matrix& m)
{
  int r = m.get_row_dimension();
  int c = m.get_column_dimension();
  data = new float *[r];
  for (int i = 0; i < r; i++)
    data[i] = new float[c];
  for (int i = 0; i < r; i++)
    for (int j = 0; j < c; j++)
      data[i][j] = 0;
  this->column = c; this->row = r;
}

Matrix::Matrix(Matrix&& m)
{
  int r = m.get_row_dimension();
  int c = m.get_column_dimension();
  data = m.data;
  m.data = NULL;
}

Matrix& Matrix::operator= (const Matrix& m){
  // overload = for assignment
  if(this == &m)
    return *this;
  int r = m.get_row_dimension();
  int c = m.get_column_dimension();
  if (row != r || column != c)
    data = new float *[r];
  for (int i=0; i < row; ++i)
    for (int j=0; j < column; ++j)
      data[i][j] = m.get_entry(i, j);
}

ostream & operator<<(ostream & cout,  const Matrix & m)
{
  for (int i = 0; i < m.row; i++)
  {
    for (int j = 0; j < m.column; j++)
      cout << m.data[i][j] << " ";
    cout << endl;
  }
  
  return cout;
}

istream & operator>>(istream & cin, Matrix &m)
{
  for (int i = 0; i < m.row; i++)
    for (int j = 0; j < m.column; j++)
      cin >> m.data[i][j];
  return cin;
}

float Matrix::get_entry(int r, int c) const
{
  if (r > this->row-1 or c > this->column-1)
    cout << "Index out of range!" << endl;
  return this->data[r][c];
}

int Matrix::get_row_dimension() const
{
  return row;
}

int Matrix::get_column_dimension() const
{
  return column;
}

void Matrix::row_switching(int a, int b)
{
  if (a > this->row-1 or b > this->row-1) 
    cout << "Index out of range!" << endl;
  for (int i = 0; i < this->column; i++)
  {
    float temp = this->data[a][i];
    this->data[a][i] = this->data[b][i];
    this->data[b][i] = temp;
  }
}

void Matrix::row_multiplication(int a, float k)
{
  if (k == 0)
    cout << "Cannot be multiplied by a zero constant!" << endl;
  for (int i = 0; i < this->column; i++)
    this->data[a][i] *= k;
}

void Matrix::row_addition(int a, int b, float k)
{
  if (a==b)
    cout << "Cannot be the same row!" << endl;
  for (int i = 0; i < this->column; i++)
    this->data[a][i] += k*this->data[b][i];
}

void Matrix::transpose()
{
  int newC = this->row; int newR = this->column;
  float **new_data;
  new_data = new float *[newR];
  for (int i = 0; i < newR; i++)
    new_data[i] = new float[newC];
  for (int i = 0; i < newR; i++)
    for (int j = 0; j < newC; j++)
      new_data[i][j] = this->data[j][i];
  this->data = new_data;
  this->row = newR; this->column = newC;
}

Matrix operator+(const Matrix & m, const Matrix &n)
{
  if ((m.row != n.row) || (m.column != n.column))
    cout << "Cannot do the matrix addition!" << endl;
  Matrix k(m.row,m.column);
  for (int i = 0; i < m.row; i++)
    for (int j = 0; j < m.column; j++)
      k.data[i][j] = m.data[i][j] + n.data[i][j];
  return k;
}

Matrix operator*(const Matrix & m, const float & k)
{
  Matrix result(m.row,m.column);
  for (int i = 0; i < m.row; i++)
    for (int j = 0; j < m.column; j++)
      result.data[i][j] = m.data[i][j]*k;
  return result;
}

Matrix operator*(const float & k, const Matrix & m)
{
  Matrix result(m.row,m.column);
  for (int i = 0; i < m.row; i++)
    for (int j = 0; j < m.column; j++)
      result.data[i][j] = m.data[i][j]*k;
  return result;
}

Matrix operator*(const Matrix & m, const Matrix & n)
{
  if ((m.row != n.column) || (m.column != n.row))
    cout << "Cannot do the matrix multiplication!" << endl;
  int length = m.row;
  int width = m.column;
  Matrix result(length,length);
  for (int i = 0; i < length; i++)
  {
    for (int j = 0; j < length; j++)
    {
      float sum = 0;
      for (int a = 0; a < width; a++)
        sum += m.data[i][a] * n.data[a][j];
      result.data[i][j] = sum;
    }
  }
  return result;
}



