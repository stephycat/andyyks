#ifndef MATRIX_H
#define MATRIX_H

#include <iostream>
#include <string>
using namespace std;

class Matrix
{
    float ** data;
    int row;
    int column;

  public:
    Matrix();
    Matrix(int r, int c);
    Matrix(const Matrix& m);
    Matrix(Matrix&& m);  
    Matrix& operator= (const Matrix& m);
    friend ostream & operator<<(ostream & cout,  const Matrix & m);
    friend istream & operator>>(istream & cin, Matrix & m);
    friend Matrix operator+(const Matrix & m, const Matrix & n);
    friend Matrix operator*(const Matrix & m, const float & k);
    friend Matrix operator*(const float & k, const Matrix & m);
    friend Matrix operator*(const Matrix & m, const Matrix & n);
    float get_entry(int r, int c) const;
    int get_row_dimension() const;
    int get_column_dimension() const;
    void row_switching(int a, int b);
    void row_multiplication(int a, float k);
    void row_addition(int i, int j, float k);
    void transpose();
};

#endif


