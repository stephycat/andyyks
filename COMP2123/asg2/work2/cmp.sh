#!/bin/bash
clang++ -c -std=c++11 optClient_cmp1.cpp
clang++ MatrixExpr.o optClient_cmp1.o -o optClient_cmp1
echo "Run cmp1 for 100 times"
now=$(date +"%s") 
for i in {1..100}
do
 ./optClient_cmp1 < input_cmp.in > cmp1.out
done
let now=$(date +"%s")-now
echo "The time cost is $now"

clang++ -c -std=c++11 optClient_cmp2.cpp
clang++ MatrixExpr.o optClient_cmp2.o -o optClient_cmp2 
echo "Run cmp2 for 100 times"
now=$(date +"%s") 
for i in {1..100}
do
 ./optClient_cmp2 < input_cmp.in > cmp2.out
done
let now=$(date +"%s")-now
echo "The time cost is $now"

