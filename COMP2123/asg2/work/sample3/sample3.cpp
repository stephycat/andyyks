#include <iostream>
#include "Matrix.h"
#include "Equation_solving.h"

using namespace std;

int main()
{
  Matrix m1 (2,3); // set 3 * 4 matrix
  cin >> m1;
  for (int i=0; i<100000; i++)
    m1.transpose();
  cout << endl << "cout<<m1:" << endl << m1 << endl; // display m1
  cout << "cout<<m1.get_entry(0,2):" << endl << m1.get_entry(0,2) << endl << endl;
  cout << "cout<<m1.get_entry(0,0):" << endl << m1.get_entry(0,0) << endl << endl;
  cout << "cout<<m1.get_row_dimension():" << endl << m1.get_row_dimension() << endl << endl;
  cout << "cout<<m1.get_column_dimension():" << endl << m1.get_column_dimension() << endl << endl;

  return 0;
}

