// This program needs you to finish!
#include <algorithm>
#include <iostream>
#include <cstring>
#include <string>
#include <cstdlib>
#include "Matrix.h"

using namespace std;

Matrix::Matrix(){
  // the default constructor
  is_transposed = false;
}

Matrix::Matrix(int r, int c):Matrix()
{
  // a constructor which calls the default constructor
  if (r > 0 && c > 0)
  {
    rows = r;
    cols = c;
    data_original = (float***) malloc(rows * sizeof(float**));
    data_transposed = (float***) malloc(cols * sizeof(float**));
    for (int i=0; i < r; ++i)
    {
      data_original[i] = (float**) malloc(cols * sizeof(float*));
      for (int j=0; j < c; ++j)
        data_original[i][j] = (float*) malloc(sizeof(float));
    }
    for (int j=0; j < c; ++j)
    {
      data_transposed[j] = (float**) malloc(rows * sizeof(float*));
      for (int i=0; i < r; ++i)
        data_transposed[j][i] = data_original[i][j];
    }
    data = data_original;
  }
  else
    cout << "Invalid number of rows or columns!" << endl;
}

Matrix::Matrix(const Matrix& m)
{
  // a copy constructor
  is_transposed = false;
  rows = m.get_row_dimension();
  cols = m.get_column_dimension();
  data_original = (float***) malloc(rows * sizeof(float**));
  data_transposed = (float***) malloc(cols * sizeof(float**));
  for (int r=0; r < rows; data_original[r++] = (float**) malloc(cols * sizeof(float*)));
  for (int c=0; c < cols; data_transposed[c++] = (float**) malloc(rows * sizeof(float*)));
  for (int r=0; r < rows; ++r)
  {
    for (int c=0; c < cols; ++c)
    {
      data_original[r][c] = (float*) malloc(sizeof(float));
      *data_original[r][c] = m.get_entry(r, c);
    }
  }
  for (int c=0; c < cols; ++c)
    for (int r=0; r < rows; ++r)
      data_transposed[c][r] = data_original[r][c];
  data = data_original;
}

Matrix::Matrix(Matrix&& m)
{
  rows = m.get_row_dimension();
  cols = m.get_column_dimension();
  data_original = m.data_original;
  data_transposed = m.data_transposed;
  data = m.data;
  m.data_original = nullptr;
  m.data_transposed = nullptr;
  m.data = nullptr;
}

Matrix& Matrix::operator= (const Matrix& m){
  // overload = for assignment
  if(this == &m)
    return *this;
  int new_rows = m.get_row_dimension();
  int new_cols = m.get_column_dimension();
  if (rows != new_rows || cols != new_cols)
  {
    rows = new_rows;
    cols = new_cols;
    free(data_original);
    free(data_transposed);
    data_original = (float***) malloc(rows * sizeof(float**));
    data_transposed = (float***) malloc(cols * sizeof(float**));
    for (int r=0; r < rows; ++r)
    {
      data_original[r] = (float**) malloc(cols * sizeof(float*));
      for (int c=0; c < cols; ++c)
        data_original[r][c] = (float*) malloc(sizeof(float));
    }
    for (int c=0; c < cols; ++c)
    {
      data_transposed[c] = (float**) malloc(rows * sizeof(float*));
      for (int r=0; r < rows; ++r)
        data_transposed[c][r] = data_original[r][c];
    }
    data = data_original;
  }
  for (int r=0; r < rows; ++r)
    for (int c=0; c < cols; ++c)
      *data[r][c] = m.get_entry(r, c);
}

Matrix::~Matrix(){
  // the destructor
  free(data_original);
  free(data_transposed);
}

int Matrix::get_row_dimension() const
{
  // a const member function
  return rows;
}

int Matrix::get_column_dimension() const
{
  // a const member function
  return cols;
}

float Matrix::get_entry(int r, int c) const
{
  if (0<=r && r<rows && 0<=c && c<cols)
  {
    return *data[r][c];
  }
  else
  {
    cout << "Index out of range!" << endl;
    cout.rdbuf(NULL);
  }
}

void Matrix::row_switching(int i, int j)
{
  if (0<=i && i<rows && 0<=j && j<rows)
  {
    for (int c=0; c < cols; ++c)
    {
      float temp;
      temp = *data[i][c];
      *data[i][c] = *data[j][c];
      *data[j][c] = temp;
    }
  }
  else
  {
    cout << "Index out of range!" << endl;
    cout.rdbuf(NULL);
  }
}

void Matrix::row_multiplication(int i, float k)
{
  if (0<=i && i<rows)
  {
    if (k != 0)
    {
      for (int c=0; c < cols; ++c)
        *data[i][c] *= k;
    }
    else
    {
      cout << "Cannot be multiplied by a zero constant!" << endl;
      cout.rdbuf(NULL);
    }
  }
  else
  {
    cout << "Index out of range!" << endl;
    cout.rdbuf(NULL);
  }
}

void Matrix::row_addition(int i, int j, float k)
{
  if (0<=i && i<rows && 0<=j && j<rows)
  {
    if (i != j)
    {
      for (int c=0; c < cols; ++c)
        *data[i][c] += *data[j][c]*k;
    }
    else
    {
      cout << "Cannot be the same row!" << endl;
      cout.rdbuf(NULL);
    }
  }
  else
  {
    cout << "Index out of range!" << endl;
    cout.rdbuf(NULL);
  }
}

void Matrix::transpose()
{
  is_transposed = !is_transposed;
  swap(rows, cols);
  data = (is_transposed ? data_transposed : data_original);
}

void Matrix::transpose_prime()
{
  transpose();
}

istream & operator>> (istream& cin, Matrix& m)
{
  // overload >> for input
  for (int r=0; r < m.get_row_dimension(); ++r)
    for (int c=0; c < m.get_column_dimension(); ++c)
      cin >> *m.data[r][c];

  return cin;
}

ostream & operator<< (ostream& cout, const Matrix& m)
{
  // overload << for output
  for (int r=0; r < m.get_row_dimension(); ++r)
  {
    for (int c=0; c < m.get_column_dimension(); ++c)
      cout << *m.data[r][c] << " ";
    cout << endl;
  }
  return cout;
}

Matrix operator+ (const Matrix& m1, const Matrix& m2)
{
  // overload + for addition
  int m1_rows = m1.get_row_dimension(), m1_cols = m1.get_column_dimension();
  int m2_rows = m2.get_row_dimension(), m2_cols = m1.get_column_dimension();

  if (m1_rows == m2_rows && m1_cols == m2_cols)
  {
    Matrix matrix(m1_rows, m1_cols);
    for (int r=0; r < m1_rows; ++r)
      for (int c=0; c < m1_cols; ++c)
        *matrix.data[r][c] = m1.get_entry(r, c) + m2.get_entry(r, c);
    return matrix;
  }
  else
  {
    cout << "Cannot do the matrix addition!" << endl;
    cout.rdbuf(NULL);
    return Matrix(1, 1);
  }
}

Matrix operator* (const float scalar, const Matrix& m)
{
  // overload * for scalar multiplication c*M
  int m_rows = m.get_row_dimension(), m_cols = m.get_column_dimension();
  Matrix matrix(m_rows, m_cols);
  for (int r=0; r < m_rows; ++r)
    for (int c=0; c < m_cols; ++c)
      *matrix.data[r][c] = scalar * m.get_entry(r, c);
  return matrix;
}

Matrix operator* (const Matrix& m, const float scalar)
{
  // overload * for scalar multiplication M*c
  int m_rows = m.get_row_dimension(), m_cols = m.get_column_dimension();
  Matrix matrix(m_rows, m_cols);
  for (int r=0; r < m_rows; ++r)
    for (int c=0; c < m_cols; ++c)
      *matrix.data[r][c] = scalar * m.get_entry(r, c);
  return matrix;
}

Matrix operator* (const Matrix& m1, const Matrix& m2)
{
  // overload * for matrix multiplication
  int m1_rows = m1.get_row_dimension(), m1_cols = m1.get_column_dimension();
  int m2_rows = m2.get_row_dimension(), m2_cols = m2.get_column_dimension();

  if (m1_cols == m2_rows)
  {
    float sum;
    Matrix matrix(m1_rows, m2_cols);
    for (int r=0; r < m1_rows; ++r)
    {
      for (int c=0; c < m2_cols; ++c)
      {
        sum = 0.0;
        for (int cn=0; cn < m1_cols; ++cn)
          sum += m1.get_entry(r, cn) * m2.get_entry(cn, c);
        *matrix.data[r][c] = sum;
      }
    }
    return matrix;
  }
  else
  {
    cout << "Cannot do the matrix multiplication!" << endl;
    cout.rdbuf(NULL);
    return Matrix(1, 1);
  }
}
