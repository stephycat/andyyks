#include <iostream>
#include <string>
#include <algorithm>
#include <map>
#include <vector>
#include <list>
#include <queue>
#include <stack>
#include <stdio.h>
using namespace std;

class Node
{
  public:
    Node();
    Node(int, string);
    int id;
    string name;
};

Node::Node()
{
  id = 0;
  name = "invalid";
}

Node::Node(int input_id, string input_name)
{
  id = input_id;
  name = input_name;
}

class Graph
{
  public:
    Graph();
    void InsertNode(Node x);
    void InsertEdge(int x, int y);
    void CommonNeighbor(int x, int y);
    void ShortestPath(int source, int destination);

  private:
    map<int, Node> nodes;
    map<int, vector<int> > edges;
};

Graph::Graph() { }

void Graph::InsertNode(Node x)
{
  if (nodes.count(x.id) > 0)
  {
    cout << "ID exists." << endl;
    return;
  }
  else
  {
    nodes[x.id] = x;
  }
}

void Graph::InsertEdge(int x, int y)
{
  if (nodes.count(x) == 0 || nodes.count(y) == 0)
  {
    cout << "No such node." << endl;
    return;
  }

  if ( edges.count(x)  == 0 )
  {
    vector<int> temp;
    edges[x] = temp;
  }

  edges[x].push_back(y);
}

void Graph::CommonNeighbor(int x, int y)
{
  if (nodes.count(x) == 0 || nodes.count(y) == 0)
  {
    cout << "No such node." << endl;
    return;
  }

  vector<int> xNeighbors;
  vector<int> yNeighbors;

  map<int, vector<int> >::iterator itx = edges.find(x);

  if ( itx == edges.end() )
  {
    perror("Invoking CommonNeighbor on x which does not have any outgoing edge");
    return;
  }
  else
  {
    xNeighbors = itx -> second;
  }

  map<int, vector<int> >::iterator ity = edges.find(y);

  if ( ity == edges.end() )
  {
    perror("Invoking CommonNeighbor on y which does not have any outgoing edge");
    return;
  }
  else
  {
    yNeighbors = ity -> second;
  }

  sort(xNeighbors.begin(), xNeighbors.end());
  sort(yNeighbors.begin(), yNeighbors.end());

  vector<int> commonNeighbor;

  set_intersection(xNeighbors.begin(), xNeighbors.end(), yNeighbors.begin(), yNeighbors.end(), back_inserter(commonNeighbor));

  if (commonNeighbor.size() == 0)
  {
    cout << "No common neighbor." << endl;
    return;
  }
  else
  {
    for (vector<int>::iterator it = commonNeighbor.begin(); it != commonNeighbor.end(); it++)
    {
      cout << *it << ' ' << nodes[*it].name << endl; 
    }
  }
}

void Graph::ShortestPath(int source, int destination)
{
  if (source == destination)
  {
    cout << source << ' ' << nodes[source].name << endl;
    return;
  }

  queue<int> q;
  map<int, int> previous;
  map<int, bool> visited;

  for (map<int, Node>::iterator it = nodes.begin(); it != nodes.end(); it++)
  {
    visited[it -> first] = false;
    previous[it -> first] = -1;
  }

  q.push(source);
  visited[source] = true;

  bool destinationFound = false;

  while (q.size() > 0)
  {
    if (destinationFound)
      break;

    int current = q.front();
    q.pop();

    for (vector<int>::iterator it = edges[current].begin(); it != edges[current].end(); it++)
    {
      if ( visited[*it] == false )
      {
          q.push(*it);
          visited[*it] = true;
          previous[*it] = current;
      }
      if (*it == destination)
      {
        destinationFound = true;
        break;
      }
    }
  }
  if (destinationFound == false)
  {
    cout << "No path found." << endl;
    return;
  }

  stack<int> path;
  path.push(destination);

  int temp = destination;
  while (previous[temp] != source)
  {
    path.push(previous[temp]);
    temp = previous[temp];
  }

  path.push(source);

  while (path.size() > 0)
  {
    int temp = path.top();
    cout << temp << ' ' << nodes[temp].name << endl;
    path.pop();
  }
}


int main()
{
  Graph g;
  string command;
  int id1, id2;
  string name;

  while (cin >> command)
  {
    if (command == "InsertNode")
    {
      cin >> id1 >> name;
      Node n(id1, name);
      g.InsertNode(n);
    }
    if (command == "InsertEdge")
    {
      cin >> id1 >> id2;
      g.InsertEdge(id1, id2);
    }
    else if (command == "CommonNeighbor")
    {
      cin >> id1 >> id2;
      g.CommonNeighbor(id1, id2);
    }
    else if (command == "ShortestPath")
    {
      cin >> id1 >> id2;
      g.ShortestPath(id1, id2);
    }
    else if (command == "Exit")
    {
      return 0;
    }
  }
}
