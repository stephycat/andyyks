#include<iostream>
#include<string>
#include<algorithm>
#include<map>
#include<vector>
using namespace std;

class student{
public:
    student();
    student(int,string);
    int id;
    string name;
};

student ::student(){
    id=0;
    name="invalid";
}
student::student(int inputid, string inputname){
    id=inputid;
    name=inputname;
}

class table{
public:
    
    void InsertStudent(student x,int y);
    
    void SearchbyID(student x);
    
    void SearchbyGrade(int y);
    
    void SortbyGrade(table t);
    
    void PrintAll(table t);
    
private:
    map<student,int> records;

};

bool operator<(const student&a,const student&b){
    return a.id<b.id;
}


//insert one record in to the map
void table:: InsertStudent(student x,int y){
    if (records.count(x)>0){
        cout << "student exists."<<endl;
        return;
    }else{
        records[x]=y;
    }
}

//return the name and grade of the student with id x.id
void table:: SearchbyID(student x){
    if(records.count(x)==0){
        cout << "No such student." << endl;
        return;
    }else{
        map <student,int>::iterator itr;
        for (itr=records.begin();itr!=records.end();itr++){
            if((*itr).first.id==x.id){
                cout << (*itr).first.name <<endl;
            }
        }
        cout << records[x]<< endl;
    }
}

//return the id and name of the student with grade y
void table:: SearchbyGrade(int y){
    int flag=0;
    map <student,int>::iterator itr;
    for (itr=records.begin();itr!=records.end();itr++){
        if((*itr).second==y){
            flag=1;
            cout << (*itr).first.id << " " << (*itr).first.name <<endl;
        }
    }
    if (flag==0){
        cout << "No such record." << endl;
    }
}

//Print all records in the accending order of id
void table:: PrintAll(table t){
    map <student,int>::iterator itr;
    for (itr=records.begin();itr!=records.end();itr++){
        
            cout << (*itr).first.id << " " << (*itr).first.name << " " <<(*itr).second <<endl;
        }
}

//Print all records in the decending order of grade
typedef pair<student,int>PAIR;

int cmp(const PAIR& x, const PAIR& y){
    return x.second > y.second;
}

void table:: SortbyGrade(table t){
    map <student,int>::iterator itr;
    vector<PAIR>v;
    vector<PAIR>::iterator curr;
    for(itr=records.begin();itr!=records.end();itr++) {
        v.push_back(make_pair((*itr).first,(*itr).second));
    }
    sort(v.begin(),v.end(),cmp);
    
    for(curr=v.begin(); curr!=v.end();curr++)
    {
        cout << (*curr).first.id << " " << (*curr).first.name << " " <<(*curr).second <<endl;
    }
}


int main (){
    table t;
    string command;
    int id;
    string name;
    int grade;
    student x;
    while ( cin >> command ){
        if (command=="InsertStudent"){
            cin >> id >> name>> grade;
            student s(id,name);
            t.InsertStudent(s,grade);
        }else if (command == "SearchbyID"){
            cin >> x.id;
            t.SearchbyID(x);
        }else if (command == "SearchbyGrade"){
            cin >> grade;
            t.SearchbyGrade(grade);
        }else if (command == "PrintAll"){
            t.PrintAll(t);
        }else if (command == "SortbyGrade"){
            t.SortbyGrade(t);
        }else if (command == "exit"){
            return 0;
        }
    }
}