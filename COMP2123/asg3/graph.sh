#!/bin/bash

clear
g++ graph.cpp -o graph.o

if [ -f graph.o ]
then
  echo "Input 1:"
  ./graph.o < input.in
  rm -f graph.o
fi

