#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h> // For the key word "bool", if needed.
#define MAX 100

char * longestPalSubstr( char *inputStr );

char * longestPalSubstr( char *inputStr )
{
  int i, j, k;
  int n = strlen( inputStr ); // get length of input string
 
  // table[i][j] will be false if substring inputStr[i..j]
  // is not palindrome.
  // Else table[i][j] will be true
  bool table[n][n];
  memset(table, 0, sizeof(table));

  // All substrings of length 1 are palindromes
  int maxLength = 1;
  for (i = 0; i < n; ++i)
      table[i][i] = true;

  // check for sub-string of length 2.
  int start = 0;
  for (i = 0; i < n-1; ++i)
  {
      if (inputStr[i] == inputStr[i+1])
      {
          table[i][i+1] = true;
          start = i;
          maxLength = 2;
      }
  }

  // Check for lengths greater than 2. k is length
  // of substring
  for (k = 3; k <= n; ++k)
  {
      // Fix the starting index
      for (i = 0; i < n-k+1 ; ++i)
      {
          // Get the ending index of substring from
          // starting index i and length k
          int j = i + k - 1;

          // checking for sub-string from ith index to
          // jth index iff inputStr[i+1] to inputStr[j-1] is a
          // palindrome
          if (table[i+1][j-1] && inputStr[i] == inputStr[j])
          {
              table[i][j] = true;

              if (k > maxLength)
              {
                  start = i;
                  maxLength = k;
              }
          }
      }
  }

  char* outputStr = (char*) malloc(sizeof(char) * (start + maxLength - 1));
  for (i = 0; i < start + maxLength; i++)
    outputStr[i] = inputStr[i + start];

  return outputStr; // return length of LPS
}

int main()
{ // Do NOT modify the main function.
  char a[MAX];
  scanf("%s",a);

  char *b = longestPalSubstr(a);
  printf("%s",b);

  return 0;
}
