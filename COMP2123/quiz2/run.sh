#!/bin/bash

g++ $1.cpp -o $1.o

if [ -f $1.o ]
then
  ./$1.o
  rm -f $1.o
fi

