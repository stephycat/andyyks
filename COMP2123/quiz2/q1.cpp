#include <iostream>
#include <cstdlib>
#include <string>
#include <algorithm>
#include <map>
#include <vector>
#include <list>
#include <queue>
#include <stack>
#include <stdio.h>
using namespace std;

class Customer
{
  public:
    string name;
    vector<string> product_list;
    Customer();
    Customer(string);

    void purchase(string product_name)
    {
      product_list.push_back(product_name);
    }
};

Customer::Customer() { }

Customer::Customer(string input_name)
{
  name = input_name;
}

string cmd;
string arg1, arg2;
vector<Customer> customer_list;
bool isNew;

int main()
{
  while (true)
  {
    isNew = true;
    cin >> cmd >> arg1 >> arg2;

    if (cmd == "PURCHASE")
    {
      Customer *current_customer = new Customer(arg1);
      for (int i=0; i < customer_list.size(); i++)
      {
        if (customer_list[i].name == arg1)
        {
          current_customer = &(customer_list[i]);
          isNew = false;
          break;
        }
      }
      if (isNew)
      {
        customer_list.push_back(*current_customer);
      }

      current_customer->purchase(arg2);
    }
    else if (cmd == "HISTORY")
    {
      Customer *current_customer;
      int product_count = 0;
      for (int i=0; i < customer_list.size(); i++)
      {
        if (customer_list[i].name == arg1)
        {
          current_customer = &customer_list[i];
          break;
        }
      }
      if (current_customer == NULL)
      {
        cout << 0 << endl;
      }
      else
      {
        for (int i=0; i < current_customer->product_list.size(); i++)
        {
          if (current_customer->product_list[i] == arg2)
            ++product_count;
        }
        cout << product_count << endl;
      }
    }
    else if (cmd == "SIMILAR")
    {

    }
    else if (cmd == "SUGGEST")
    {

    }
  }
  return EXIT_SUCCESS;
}
