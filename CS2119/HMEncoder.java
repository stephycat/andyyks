import java.io.File;
import java.io.FileNotFoundException;
import java.util.concurrent.ConcurrentHashMap;
import java.util.Collections;
import java.util.IllegalFormatCodePointException;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Scanner;
import java.util.UUID;

class HMEncoder {
  public static void main (String args[]) {
    String inFilePath = args[0];
    try {
      char[] fileContent = new Scanner(new File(inFilePath), "UTF-8").useDelimiter(UUID.randomUUID().toString()).next().toCharArray();
      ConcurrentHashMap<Character, Integer> charOccuranceMap = new ConcurrentHashMap<Character, Integer>();
      for (char c : fileContent)
        charOccuranceMap.put(c, Optional.ofNullable(charOccuranceMap.get(c)).orElse(0) + 1);
      for (Character key : charOccuranceMap.keySet())
        System.out.printf("%c: %d\n", key, charOccuranceMap.get(key));
    } catch (FileNotFoundException e) {
      System.err.println(e);
    }
  }
}
