import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * 
 * @author Leung Ka Wai 3035050250
 *
 */
public class hmencoder {

	private class node{
		private node left,right;
		private char character;
		private ArrayList<Integer> location;
		private int frequency;
		String code;
		
		public node(){
			
		}
		
		public node(char in){
			this.character = in;
			location = new ArrayList<Integer>();
			//System.out.println(in);
		}
	}
	
//	private int count;
	private String input;
	private ArrayList<Integer> frequencyArray;
	private ArrayList<node> list = new ArrayList<node>();
	
	private void breakDown(){
		for (int i = 0; i < input.length(); i++){
			int locate = locateChar(input.charAt(i));
			//System.out.println(locate);
			if ( locate != -1){
				list.get(locate).location.add(i);
				list.get(locate).frequency++;
			}
			else{
				node newNode = new node(input.charAt(i));
				newNode.location.add(i);
				newNode.frequency++;
				list.add(newNode);
			//	count++;
			}
				
		}
	}
	
	private int locateChar(Character a){
		for (int i = 0; i < list.size(); i++){
			if (a.equals(list.get(i).character))
				return i;
		}
		return -1;
	}
	
	private void constructFrequencyArray(){
		/*int[] array = new int[list.size()];
		for (int i = 0; i < list.size(); i++){
			array[i] = list.get(i).frequency;
		}
		Arrays.sort(array);
		frequencyArray = new ArrayList<Integer>();
		
		for (int element:array){
			frequencyArray.add((Integer) element);
			//debug
			//System.out.print(element+" ");
		}
		//debug
		//System.out.println(frequencyArray.size());*/
		frequencyArray = new ArrayList<Integer>();
		for (int i = 0; i < list.size(); i++){
			frequencyArray.add(list.get(i).frequency);
		}
		Collections.sort(frequencyArray);
	}
	
	private void formTree(){
		while (frequencyArray.size() > 1){
			int smallest  = frequencyArray.remove(0);
			int secondSmallest = frequencyArray.remove(0);
			int position1 = findFirstNodeWithFrequency(smallest,-1);
			int position2 = findFirstNodeWithFrequency(secondSmallest,position1);
			//debug
			//System.out.println(position1+" "+position2);
			node newNode = new node();
			if (position1 > position2){
				newNode.left = getFirstNodeWithFrequency(secondSmallest);
				newNode.right = getFirstNodeWithFrequency(smallest);
			}
			else{
				newNode.left = getFirstNodeWithFrequency(smallest);
				newNode.right = getFirstNodeWithFrequency(secondSmallest);
			}
			//debug
			//System.out.print(newNode.left.character);
			//System.out.println(newNode.right.character);
			newNode.frequency = newNode.left.frequency + newNode.right.frequency;
			list.add(Math.min(position1,position2),newNode);
			frequencyArray.add(newNode.frequency);
			Collections.sort(frequencyArray);
		}
	}
	
	
	private int findFirstNodeWithFrequency(int frequency, int skipThisIndex){
		for (int i = 0; i < list.size(); i++){
			if (list.get(i).frequency == frequency && i != skipThisIndex)
				return i;
		}
		return -1;
	}
	
	private node getFirstNodeWithFrequency(int frequency){
		for (int i = 0; i < list.size(); i++){
			if (list.get(i).frequency == frequency)
					return list.remove(i);
		}
		return null;
	}
	
	private void giveCode(node n){
		if (n.left==null && n.right==null){
			n.code = "0";
		}
		else {
			n.code = "";
			ArrayList<node> tempList = new ArrayList<node>();
			tempList.add(n);
			while (tempList.size() > 0) {
				node temp = tempList.remove(0);
				if (temp.left != null) {
					temp.left.code = temp.code + "0";
					tempList.add(temp.left);
				}
				if (temp.right != null) {
					temp.right.code = temp.code + "1";
					tempList.add(temp.right);
				}
			}
		}
	}
	
	private double calAverageBits(ArrayList<node> copy){
		double output = 0;
		for (node element:copy){
			output += element.code.length()*element.location.size();
		}
		output /= input.length();
		return output;
	}
	
	
	private void go() throws IOException{
		//input = "ACCGgtGAu+g";
		BufferedReader cin = new BufferedReader(new InputStreamReader(System.in));
		input = cin.readLine();
		breakDown();
		ArrayList<node> copy = (ArrayList<node>) list.clone();
		String[] output = new String[input.length()];
		constructFrequencyArray();
		formTree();
		giveCode(list.get(0));
		list = null;
		frequencyArray = null;
		Collections.sort(copy, new Comparator<node>() {

			@Override
			public int compare(node o1, node o2) {
				return Character.compare(o1.character, o2.character);
			}

		});
		for (node element : copy) {
			System.out.println(element.character + ":" + element.code);
			for (int position : element.location)
				output[position] = element.code;
		}
		System.out.printf("%.2f%n", calAverageBits(copy));
		for (String element : output) {
			System.out.print(element);
		}
		System.out.println();

	}
	
	
	public static void main(String[] args) throws IOException{
		//while (true){
			new hmencoder().go();
		//}
	}


}