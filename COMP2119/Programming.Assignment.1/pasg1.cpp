#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <string>
#include <cstring>
#include <cassert>
#include <vector>
#include <map>
#include <set>
#include <queue>
#include <cctype>
#include <climits>
#include <sstream>

using namespace std;

/**
 * Restore the edges that linked with some other node,
 * using the way like singly-linked list
 */
typedef struct Edge {
    int vertex;
    int weight;
    Edge *next;

    Edge() {}

    Edge(int v, int w) : vertex(v), weight(w), next(NULL) {}
} Edge;

/**
 * Restore the properties of the vertex,
 * including the vertex id and the distance from the source node to it self
 */
typedef struct Node {
    int id;
    int w;

    Node() {}

    Node(int vertexID, int weight) : id(vertexID), w(weight) {}

    // comparator, when the distance is different, choose the node with smaller distance;
    // when the distance is same, choose the node with smaller vertex id
    friend bool operator<(Node a, Node b) {
        if (a.w != b.w) {
            return a.w > b.w;
        } else {
            return a.id > b.id;
        }
    }
} Node;

/**
 * Restore the attribute of the undirected graph
 */
class Graph {
public:
    // the number of nodes
    int nodesNum;
    // the number of edges
    int edgesNum;
    // the number of gas stations
    int gasStationsNum;

    // adjacency list of the graph
    map<int, Edge *> adjustList;
    // set of gas stations' ids
    set<int> gasStations;

    Graph(int n, int m, int k);

    void insertGasStation(int id);

    void insertEdge(int a, int b, int w);
};

Graph::Graph(int n, int m, int k) {
    nodesNum = n;
    edgesNum = m;
    gasStationsNum = k;
    // initializing the graph
    for (int i = 1; i <= n; i++) {
        adjustList[i] = NULL;
    }
}

void Graph::insertGasStation(int id) {
    gasStations.insert(id);
}

void Graph::insertEdge(int a, int b, int w) {
    Edge *edge = new Edge(b, w);
    if (adjustList[a] == NULL) {
        adjustList[a] = edge;
    } else {
        Edge *temp = adjustList[a];
        edge->next = temp;
        adjustList[a] = edge;
    }
}

/**
 * Single-source shortest path using dijkstra algorithm,
 * optimize using priority queue.
 *
 * Return a hash map whose key is the index of the vertex in the graph
 * and the value is the corresponding Node struct
 * contains the distance from the source vertex to self.
 */
map<int, Node> Dijkstra(int s, Graph graph) {
    map<int, Node> distance;
    set<int> visited;
    priority_queue<Node> que;

    int vertexNum = graph.nodesNum;
    // initializing, the distance from source vertex s to the vertex i is infinite
    for (int i = 1; i <= vertexNum; i++) {
        distance[i] = Node(i, INT_MAX);
    }

    // dijkstra algorithm with the optimization using priority queue
    distance[s].w = 0;
    que.push(distance[s]);
    while (!que.empty()) {
        Node minDisNode = que.top();
        que.pop();
        int uNode = minDisNode.id;
        if (visited.count(uNode) > 0) {
            continue;
        }
        visited.insert(uNode);
        Edge *edge = graph.adjustList[uNode];
        while (edge) {
            int vNode = edge->vertex;
            if (visited.count(vNode) == 0 &&
                distance[vNode].w > distance[uNode].w + edge->weight) {
                distance[vNode].w = distance[uNode].w + edge->weight;
                que.push(distance[vNode]);
            }
            edge = edge->next;
        }
    }
    return distance;
}

/**
 * Find the nearest gas station for the road junction v.
 *
 * Using dijkstra algorithm to find the shortest path from v to each node in the graph,
 * and looping through all the gas stations and find the station with smallest distance,
 * this is the answer.
 */
int nearestGasStation(int v, Graph g) {
    map<int, Node> distance = Dijkstra(v, g);
    set<int> gasStations = g.gasStations;
    int minDistance = INT_MAX, ans = INT_MAX;
    for (set<int>::iterator iter = gasStations.begin(); iter != gasStations.end(); ++iter) {
        if (minDistance > distance[*iter].w) {
            minDistance = distance[*iter].w;
            ans = *iter;
        } else if (minDistance == distance[*iter].w) {
            // if more gas stations that are at equal distance,
            // choose the gas station with the smaller id
            ans = min(ans, *iter);
        }
    }
    return ans;
}

/**
 * Find the number of reverse neighbors of the given gas station u.
 *
 * If the number of gas stations is k,
 * then looping through all the gas stations and for each one performing dijkstra algorithm,
 * totally we need doing Dijkstra’s only k times.
 *
 * Then for each node in the graph there's k distance,
 * choose the minimum distance and see whether it comes from station u.
 */
int calculateNumofReverseNeighbors(int u, Graph g) {
    map<int, Node> distanceFromU = Dijkstra(u, g);
    map<int, int> minDistanceSource;
    set<int> gasStations = g.gasStations;

    for (int i = 1; i <= g.nodesNum; i++) {
        minDistanceSource[i] = u;
    }
    for (set<int>::iterator iter = gasStations.begin(); iter != gasStations.end(); ++iter) {
        if (*iter == u) {
            continue;
        }
        map<int, Node> distanceFromIter = Dijkstra(*iter, g);
        for (int i = 1; i <= g.nodesNum; i++) {
            if (distanceFromIter[i].w < distanceFromU[i].w) {
                minDistanceSource[i] = *iter;
            } else if (distanceFromIter[i].w == distanceFromU[i].w && *iter < u) {
                minDistanceSource[i] = *iter;
            }
        }
    }

    int ans = 0;
    for (int i = 1; i <= g.nodesNum; i++) {
        if (minDistanceSource[i] == u) {
            ans++;
        }
    }
    return ans;
}

int main() {
    string input, newLine;
    char comma;
    stringstream ss;

    int n, m;
    getline(cin, input);
    ss.clear();
    ss.str(input);
    ss >> n >> comma >> m;

    int k;
    cin >> k;
    getline(cin, newLine);
    Graph graph(n, m, k);

    getline(cin, input);
    ss.clear();
    ss.str(input);
    for (int i = 0; i < k; i++) {
        int gasStationID;
        ss >> gasStationID;
        graph.insertGasStation(gasStationID);
        if (i != k - 1) {
            ss >> comma;
        }
    }

    for (int i = 0; i < m; i++) {
        getline(cin, input);
        ss.clear();
        ss.str(input);
        int a, b, w;
        ss >> a >> comma >> b >> comma >> w;
        graph.insertEdge(a, b, w);
        graph.insertEdge(b, a, w);
    }

    string problemSpec;
    cin >> problemSpec;
    cout << problemSpec << endl;
    if (problemSpec == "NearestGasStation") {
        int v;
        cin >> v;
        cout << nearestGasStation(v, graph) << endl;
    } else if (problemSpec == "ReverseNeighbors") {
        int u;
        cin >> u;
        cout << calculateNumofReverseNeighbors(u, graph) << endl;
    } else {
        cout << "wrong input file!" << endl;
        // assert (0);
    }
    return 0;
}

/**
4,4
2
1,2
1,2,54
2,3,54
1,3,54
1,4,52
NearestGasStation
3

4,3
3
1,2,3
1,3,54
2,3,10
2,4,54
NearestGasStation
4
*/

/**
4,4
2
2,3
1,2,54
1,3,45
2,4,12
1,4,5
ReverseNeighbors
2

4,3
2
1,2
1,3,54
1,4,54
2,3,10
ReverseNeighbors
2
*/
