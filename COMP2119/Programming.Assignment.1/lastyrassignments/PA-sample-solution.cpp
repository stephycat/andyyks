/*

compile with:
    g++ -std=c++11 -O2 PA-sample-solution.cpp -o main
run with:
    ./main < sample.inp > my.oup
compare with:
    diff -w sample.oup my.oup

*/



#include <iostream>
#include <string>
#include <queue>
#include <list>
#include <assert.h>

using namespace std;

// iPair ==>  Integer Pair
typedef pair<int, int> iPair;
 
// why INF is 0x3f3f3f3f? -> http://stackoverflow.com/questions/18429021/why-is-infinity-0x3f3f3f3f 
const int INF = 0x3f3f3f3f;
const int maxN = 505;


// global variables
// n: total number of vertices, m: total number of edges
int n, m;
// represents an "small" undirected graph using 
// adjacency matrix
int fdist[maxN][maxN];



 
// This class represents an undirected graph using
// adjacency list representation
class Graph
{
    int V;    // No. of vertices
 
    // In a weighted graph, we need to store vertex
    // and weight pair for every edge
    list< pair<int, int> > *adj;
 
public:
    Graph(int V);  // Constructor
 
    // function to add an edge to graph
    void addEdge(int u, int v, int w);
 
    // calculates shortest path from s
    void shortestPath(int s);

    vector<int> dist;
};
 
// Allocates memory for adjacency list
Graph::Graph(int V)
{
    this->V = V;
    adj = new list<iPair> [V];
}
 
void Graph::addEdge(int u, int v, int w)
{
    adj[u].push_back(make_pair(v, w));
    adj[v].push_back(make_pair(u, w));
}
 


// Program to find Dijkstra's shortest path using
// priority_queue in STL

// Prints shortest paths from src to all other vertices
void Graph::shortestPath(int src)
{
    // Create a priority queue to store vertices that
    // are being preprocessed. This is weird syntax in C++.
    // Refer below link for details of this syntax
    // http://geeksquiz.com/implement-min-heap-using-stl/
    priority_queue< iPair, vector <iPair> , greater<iPair> > pq;
 
    // Create a vector for distances and initialize all
    // distances as infinite (INF)
    //vector<int> dist(V, INF);
    dist.assign(V, INF);
    // Insert source itself in priority queue and initialize
    // its distance as 0.
    pq.push(make_pair(0, src));
    dist[src] = 0;
 
    /* Looping till priority queue becomes empty (or all
      distances are not finalized) */
    while (!pq.empty())
    {
        // The first vertex in pair is the minimum distance
        // vertex, extract it from priority queue.
        // vertex label is stored in second of pair (it
        // has to be done this way to keep the vertices
        // sorted distance (distance must be first item
        // in pair)
        int u = pq.top().second;
        pq.pop();
 
        // 'i' is used to get all adjacent vertices of a vertex
        list< pair<int, int> >::iterator i;
        for (i = adj[u].begin(); i != adj[u].end(); ++i)
        {
            // Get vertex label and weight of current adjacent
            // of u.
            int v = (*i).first;
            int weight = (*i).second;
 
            //  If there is shorted path to v through u.
            if (dist[v] > dist[u] + weight)
            {
                // Updating distance of v
                dist[v] = dist[u] + weight;
                pq.push(make_pair(dist[v], v));
            }
        }
    }
 

    // Print shortest distances stored in dist[]
    // printf("Vertex   Distance from Source\n");
    // for (int i = 0; i < V; ++i)
    //     printf("%d \t\t %d\n", i, dist[i]);
}
 


void NearestDriver(){
    // read input graph
    cin >> n >> m;
    Graph g(n);
    for(int i = 0; i < m; i++){
        int a, b, w;
        cin >> a >> b >> w;
        assert(0 <= a && a < n && 0 <= b && b < n && 0 <= w && w <= 1000);
        g.addEdge(a, b, w);
    }

    // read Mike's position
    int u;
    cin >> u;
    g.shortestPath(u);

    int bestv = -1, bestdist = INF;

    // scan over every driver
    int l;
    cin >> l;
    for(int i = 0; i < l; i++){
        int v;
        cin >> v;
        if(g.dist[v] < bestdist){
            bestv = v;
            bestdist = g.dist[v];
        }else if(g.dist[v] == bestdist){
            if(v < bestv)
                bestv = v;
        }
    }


    if(bestv == -1)
        cout << "NO" << endl;
    else
        cout << bestv << endl;
    return;
}



// helper function performing Floyd-Warshall Algorithm
void helper(){

    // Intialize 
    for(int i = 0; i < maxN; i++){
        for(int j = 0; j < maxN; j++)
            fdist[i][j] = INF;
        fdist[i][i] = 0;
    }



    cin >> n >> m;
    assert(n > 0 && n < maxN);

    // read each edge
    for(int i = 0; i < m; i++){
        int a, b, w;
        cin >> a >> b >> w;
        fdist[a][b] = fdist[b][a] = w;
    }


    for(int k = 0; k < n; k++){
        // Pick all vertices as source one by one
        for(int i = 0; i < n; i++){
            // Pick all vertices as destination for the above picked source
            for(int j = 0; j < n; j++){
                // If vertex k is on the shortest path from
                // i to j, then update the value of dist[i][j]
                if (fdist[i][k] + fdist[k][j] < fdist[i][j])
                    fdist[i][j] = fdist[i][k] + fdist[k][j];
            }
        }
    }

}

void QueryPrice(){
    
    // read graph, perform Floyd-Warshall Algorithm
    helper();


    // anwser each query
    int k;
    cin >> k;
    for(int i = 0; i < k; i++){
        int a, b, w;
        cin >> a >> b;
        if(fdist[a][b] == INF)
            cout << "NO" << endl;
        else 
            cout << fdist[a][b] << endl;
    }
}

void Diameter(){
    helper();
    

    //Diameter is the longest distance between vertices pairs

    int di = -1;
    for(int i = 0; i < n; i++)
        for(int j = 0; j < n; j++)
            di = max(di, fdist[i][j]);
    if(di == INF)
        cout << "INF" << endl;
    else 
        cout << di << endl; 
}

void DiameterApproximation(){
    
    cin >> n >> m;
    Graph g(n);
    for(int i = 0; i < m; i++){
        int a, b, w;
        cin >> a >> b >> w;
        assert(0 <= a && a < n && 0 <= b && b < n && 0 <= w && w <= 1000);
        g.addEdge(a, b, w);
    }

    // High level idea, perform two shortest path procedures
    // let s <- 0
    // find u from s
    // find v from u
    // dist(u, v)   =          D(G) if G is tree
    //              =   >= 0.5*D(G) otherwise
    g.shortestPath(0);
    int u = -1, du = -1;
    for(int i = 0; i < n; i++)
        if(g.dist[i] > du){
            u = i;
            du = g.dist[i];
        }
    g.shortestPath(u);      
    int v = -1, dv = -1;      
    for(int i = 0; i < n; i++)
        if(g.dist[i] > dv){
            v = i;
            dv = g.dist[i];
        }

    cout << dv << endl;
}

int main(){
    string section;
    cin >> section;

    if(section == "NearestDriver")    
        NearestDriver();
    else if(section == "QueryPrice")    
        QueryPrice();
    else if(section == "Diameter")    
        Diameter();
    else if(section == "DiameterApproximation")    
        DiameterApproximation();
    else{
        cout << "wrong input file!" << endl;
        assert(0);
    }

    return 0;
}
