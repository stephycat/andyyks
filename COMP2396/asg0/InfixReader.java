import java.io.*;

public class InfixReader {

  public static void main(String[] args) {
    InfixReader myAnswer = new InfixReader();
		myAnswer.doConversion();
  }

  public void doConversion()
  {
    Stack stack;
    while(true)
    {
      stack = new Stack();
      String[] infixStr = readInfix();
      String result = "";

      for (String token : infixStr)
      {
        if (operator(token))
          if (")".equals(token))
          {
            while (!stack.isEmpty() && !"(".equals(stack.getTop()))
              result += stack.pop() + " ";
            if (!stack.isEmpty())
              stack.pop();
          }
          else
          {
            if (!stack.isEmpty() && !isLowerPrecedence(token, stack.getTop()))
              stack.push(token);
            else
            {
              while (!stack.isEmpty() && isLowerPrecedence(token, stack.getTop()))
              {
                String top = stack.pop();
                if (!"(".equals(top))
                  result += top + " ";
              }
              stack.push(token);
            }
          }
        else
          result += token + " ";
      }
      while (!stack.isEmpty())
        result += stack.pop() + " ";

      System.out.println("Postfix: " + result);
    }
  }

  private String [] readInfix()
  {  
    BufferedReader input = new BufferedReader(new InputStreamReader(System.in)); 
    String inputLine;
    try
    {
      System.out.print("Please input infix: ");
      inputLine = input.readLine();
      return inputLine.split(" ");
    }
    catch (IOException e)
    {
      System.err.println("Input ERROR.");
    }
    
    // return empty array if error occurs
    return new String[] { };
	}

  private boolean isLowerPrecedence(String subject, String toCompare)
  {
    switch (subject)
    {
      case "+":
        return !("+".equals(toCompare) || "(".equals(toCompare));
      case "-":
        return !("-".equals(toCompare) || "(".equals(toCompare));

      case "*":
        return "/".equals(toCompare) || "^".equals(toCompare) || "(".equals(toCompare);
      case "/":
        return "*".equals(toCompare) || "^".equals(toCompare) || "(".equals(toCompare);

      case "^":
        return "(".equals(toCompare);

      case "(":
        return false;

      default:
        return false;
    }
  }

  private boolean operator(String subject)
  {
    return "+".equals(subject) ||
           "-".equals(subject) ||
           "*".equals(subject) ||
           "/".equals(subject) ||
           "^".equals(subject) ||
           "(".equals(subject) ||
           ")".equals(subject);
  }
}

class Stack
{
  private Node head;

  private static class Node
  {
    String item;
    Node next;

    public Node(String item, Node next)
    {
      this.item = item;
      this.next = next;
    }
  }

  public void push(String item)
  {
    Node newNode = new Node(item, null);
    newNode.next = head;
    head = newNode;
  }

  public String pop()
  {
    if (isEmpty())
      return null;
    String data = head.item;
    head = head.next;
    return data;
  }

  public boolean isEmpty() 
  {
    return head == null;
  }

  public String getTop()
  {
    return head.item;
  }
}