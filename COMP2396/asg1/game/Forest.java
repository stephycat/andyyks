package game;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Scanner;

import animals.*;
import libraries.*;

/**
 * The forest
 * @author andyyks
 */
public class Forest
{
  private static final int ROWS = 12;
  private static final int COLS = 12;
  private final String EMPTY = ".";
  private LinkedHashMap<String, Animal> animals;
  private Animal board[][];

  /**
   * Creates a forest with animals at random locations
   */
  @SuppressWarnings("serial")
  public Forest()
  {
    Forest thisForest = this;
    this.board = new Animal[ROWS][COLS];

    ArrayList<Location> locations = new ArrayList<Location>();
    for (int y=0; y<ROWS; y++)
      for (int x=0; x<COLS; x++)
        locations.add(new Location(y, x));

    animals = new LinkedHashMap<String, Animal>() {
      {
        put("cat",    new Cat(thisForest));
        put("dog",    new Dog(thisForest));
        put("fox",    new Fox(thisForest));
        put("hippo",  new Hippo(thisForest));
        put("lion",   new Lion(thisForest));
        put("tiger",  new Tiger(thisForest));
        put("turtle", new Turtle(thisForest));
        put("wolf",   new Wolf(thisForest));
      }
    };

    for (Animal animal: animals.values())
      this.setAnimalAt(animal, animal.setLocation(Randomizer.draw(locations)));
  }

  /**
   * Check whether a location is empty
   * @param location Target location
   * @return True for empty, False for being occupied by an animal
   */
  public boolean isEmpty(final Location location)
  {
    return (this.getAnimalAt(location) == null);
  }

  /**
   * Check whether a location is inside the forest
   * @param location Target location
   * @return True for inside the forest, False for outside the forest
   */
  public boolean isInside(final Location location)
  {
    int y = location.get(VComponent.Y);
    int x = location.get(VComponent.X);
    return (0 <= y && y < ROWS && 0 <= x && x < COLS);
  }

  /**
   * Get the animal occupied in a specific location
   * @param location Target location
   * @return Target animal
   */
  public Animal getAnimalAt(Location location)
  {
    int y = location.get(VComponent.Y);
    int x = location.get(VComponent.X);
    return this.board[y][x];
  }

  /**
   * Set the animal to be occupied in a specific location
   * @param animal Target animal
   * @param location Target location
   */
  public void setAnimalAt(Animal animal, Location location)
  {
    int y = location.get(VComponent.Y);
    int x = location.get(VComponent.X);
    this.board[y][x] = animal;
  }

  /**
   * Runs a simulation cycle, animals will move one by one 
   */
  public void perform()
  {
    display();
    while (!getUserInstruction().toLowerCase().equals("exit"))
    {
      // System.out.println();
      for (Animal animal: animals.values())
        if (animal.isAlive())
        {
          // new Scanner(System.in).nextLine();
          animal.moveInTurn();
          // display();  // display after each turn
        }
      // display();  // display after each cycle
    }
    for (Animal animal: animals.values())
      System.out.println(
        animal.getClass().getSimpleName() + " is " +
        (animal.isAlive() ? "alive" : "dead") + " at " + 
        animal.getLocation()
      );
  }

  /**
   * Display the forest with animals
   */
  private void display()
  {
    for (Animal[] row: board)
    {
      for (Animal cell: row)
        System.out.print(cell!=null ? cell.getLabel() : EMPTY);
      System.out.println();
    }
  }

  /**
   * Acquire user input as instruction
   * @return User input
   */
  private String getUserInstruction()
  {
    System.out.print("Press enter to iterate, type 'exit' to quit: ");
    return new Scanner(System.in).nextLine();
  }
}