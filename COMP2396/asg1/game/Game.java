package game;

/**
 * The Game launcher
 * @author andyyks
 */
public class Game
{
  public static void main (String[] args)
  {
    new Forest().perform();
    System.exit(0);
  }
}
