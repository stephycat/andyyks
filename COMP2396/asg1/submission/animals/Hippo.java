package animals;

import game.*;
import libraries.*;

public class Hippo extends Animal
{
  public Hippo(Forest forest)
  {
    super("h", forest);
  }
}