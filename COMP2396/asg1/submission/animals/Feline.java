package animals;

import static java.util.Arrays.asList;
import game.*;
import libraries.*;

public abstract class Feline extends Animal
{
  public Feline(String label, Forest forest)
  {
    super(label, forest);
    this.defaultMoves.addAll(
      asList(
        new Move(Direction.NORTH_EAST, 1),
        new Move(Direction.SOUTH_EAST, 1),
        new Move(Direction.SOUTH_WEST, 1),
        new Move(Direction.NORTH_WEST, 1)
      )
    );
  }

  @Override
  public boolean attack(Animal animal)
  {
    if (animal instanceof Canine)
      return animal.die();
    else
      return super.attack(animal);
  }
}