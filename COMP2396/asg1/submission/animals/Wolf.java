package animals;

import game.*;
import libraries.*;

public class Wolf extends Canine
{
  public Wolf(Forest forest)
  {
    super("w", forest);
  }
}