package animals;

import game.*;
import libraries.*;

public class Dog extends Canine
{
  public Dog(Forest forest)
  {
    super("d", forest);
  }
}