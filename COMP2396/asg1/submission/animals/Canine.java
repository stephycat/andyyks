package animals;

import static java.util.Arrays.asList;
import game.*;
import libraries.*;

public abstract class Canine extends Animal
{
  public Canine(String label, Forest forest)
  {
    super(label, forest);
    this.defaultMoves.addAll(
      asList(
        new Move(Direction.NORTH, 2),
        new Move(Direction.EAST, 2),
        new Move(Direction.SOUTH, 2),
        new Move(Direction.WEST, 2)
      )
    );
  }

  @Override
  public boolean attack(Animal animal)
  {
    if (animal instanceof Feline)
      if (!animal.die(0.5))
        return !this.die();
      else
        return true;
    else
      return super.attack(animal);
  }
}