package animals;

import game.*;
import libraries.*;

public class Tiger extends Feline
{
  public Tiger(Forest forest)
  {
    super("t", forest);
  }
}