package animals;

import game.*;
import libraries.*;

public class Lion extends Feline
{
  public Lion(Forest forest)
  {
    super("l", forest);
  }

  @Override
  public boolean attack(Animal animal)
  {
    if (animal instanceof Hippo)
      return animal.die();
    else
      return super.attack(animal);
  }
}