package animals;

import game.*;
import libraries.*;

public class Cat extends Feline
{
  public Cat(Forest forest)
  {
    super("c", forest);
  }
}