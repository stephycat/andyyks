package animals;

import game.*;
import libraries.*;

public class Fox extends Canine
{
  public Fox(Forest forest)
  {
    super("f", forest);
  }

  @Override
  public boolean attack(Animal animal)
  {
    if (animal instanceof Cat)
      return animal.die();
    else
      return super.attack(animal);
  }
}