package animals;

import game.*;
import libraries.*;

public class Turtle extends Animal
{
  public Turtle(Forest forest)
  {
    super("u", forest);
  }

  @Override
  public boolean attack(Animal animal)
  {
    if (!animal.die(0.5))
      return !this.die();
    else
      return true;
  }
}