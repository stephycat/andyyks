package game;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Scanner;
import animals.*;
import libraries.*;

public class Forest
{
  private static final int ROWS = 12;
  private static final int COLS = 12;
  private final String EMPTY = ".";
  private LinkedHashMap<String, Animal> animals;
  private Animal board[][];

  public Forest()
  {
    Forest thisForest = this;
    this.board = new Animal[ROWS][COLS];

    ArrayList<Location> locations = new ArrayList<Location>();
    for (int y=0; y<ROWS; y++)
      for (int x=0; x<COLS; x++)
        locations.add(new Location(y, x));

    animals = new LinkedHashMap<String, Animal>() {
      {
        put("cat",    new Cat(thisForest));
        put("dog",    new Dog(thisForest));
        put("fox",    new Fox(thisForest));
        put("hippo",  new Hippo(thisForest));
        put("lion",   new Lion(thisForest));
        put("tiger",  new Tiger(thisForest));
        put("turtle", new Turtle(thisForest));
        put("wolf",   new Wolf(thisForest));
      }
    };

    for (Animal animal: animals.values())
      this.setAnimalAt(animal, animal.setLocation(Randomizer.draw(locations)));
  }

  public boolean isEmpty(final Location location)
  {
    return (this.getAnimalAt(location) == null);
  }

  public boolean isInside(final Location location)
  {
    int y = location.get(VComponent.Y);
    int x = location.get(VComponent.X);
    return (0 <= y && y < ROWS && 0 <= x && x < COLS);
  }

  public Animal getAnimalAt(Location location)
  {
    int y = location.get(VComponent.Y);
    int x = location.get(VComponent.X);
    return this.board[y][x];
  }

  public void setAnimalAt(Animal animal, Location location)
  {
    int y = location.get(VComponent.Y);
    int x = location.get(VComponent.X);
    this.board[y][x] = animal;
  }

  public void perform()
  {
    display();
    while (!getUserInstruction().toLowerCase().equals("exit"))
    {
      System.out.println();
      for (Animal animal: animals.values())
        if (animal.isAlive())
        {
          // new Scanner(System.in).nextLine();
          animal.moveInTurn();
          // display();
        }
      display();
    }
  }

  
  private void display()
  {
    for (Animal[] row: board)
    {
      for (Animal cell: row)
        System.out.print((cell!=null ? cell.getLabel() : EMPTY) + " ");
      System.out.println();
    }
  }

  private String getUserInstruction()
  {
    System.out.print("Press enter to iterate, type 'exit' to quit: ");
    return new Scanner(System.in).nextLine();
  }
}