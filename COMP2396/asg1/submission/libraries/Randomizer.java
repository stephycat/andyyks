package libraries;

import java.util.ArrayList;
import java.util.Random;

public class Randomizer
{

  public static boolean getBoolean(double probability)
  {
    if (!(0 <= probability && probability <= 1))
      return false;
    if (probability == 0)
      return false;
    if (probability == 1)
      return true;
    if (probability == 0.5)
      return (new Random()).nextBoolean();
    return new Random().nextDouble() < probability ? true : false;
  }

  public static <T> T draw(ArrayList<T> inputArray)
  {
    int arr_size = inputArray.size();
    if (arr_size > 0)
    {
      int random_index = new Random().nextInt(arr_size);
      T result = inputArray.get(random_index);
      inputArray.remove(random_index);
      return result;
    }
    return null;
  }
}