package libraries;

public class Move
{
  private Direction direction;
  private int step;

  public Move(Direction direction, int step)
  {
    this.direction = direction;
    this.step = step;
  }

  public int getMaxStep()
  {
    return this.step;
  }

  public Location seek(Location start, int step)
  {
    return start.adjacency(this.direction, Math.min(step, this.step));
  }
}