package libraries;

import java.util.LinkedHashMap;

public class Location
{
  private LinkedHashMap<VComponent, Integer> coordinate;

  public Location(int y, int x)
  {
    coordinate = new LinkedHashMap<VComponent, Integer>() {
      {
        put(VComponent.Y, new Integer(y));
        put(VComponent.X, new Integer(x));
      }
    };
  }

  public int get(VComponent c)
  {
    return this.coordinate.get(c);
  }

  public Location adjacency(Direction direction, int step)
  {
    return new Location(
      this.get(VComponent.Y) + direction.get(VComponent.Y) * step,
      this.get(VComponent.X) + direction.get(VComponent.X) * step
    );
  }

  @Override
  public String toString()
  {
    return get(VComponent.Y) + ", " + get(VComponent.X);
  }
}