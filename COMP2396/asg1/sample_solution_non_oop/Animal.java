import java.util.Random;

public class Animal{
	protected int x;
	protected int y;
	protected String name;
	protected String label;
	protected boolean alive = true;
	protected int newX;
	protected int newY;
	protected static Random rand;
	protected static String[][] forestNow;
	protected Animal[] myAnimals;
	void moveTo(){
		do{
			rand = new Random();
			int ran = rand.nextInt(4);
			//left,right,up,down = [0,1,2,3]
			switch(ran){
				case 0:
					newX = x - 1;
					newY = y;
					break;
				case 1:
					newX = x + 1;
					newY = y;
					break;
				case 2:
					newX = x;
					newY = y - 1;
					break;
				case 3:
					newX = x;
					newY = y + 1;
					break;
				default:
					break;
			}
		}while (newX > 11 || newY < 0 || newX < 0 || newY > 11);
	}

	void dieTo(){
		do{
			rand = new Random();
			int ran = rand.nextInt(8);
			//left,right,up,down = [0,1,2,3], topleft 4, topright 5, bottomleft 6, bottomright 7
			switch(ran){
				case 0:
					newX = x - 1;
					newY = y;
					break;
				case 1:
					newX = x + 1;
					newY = y;
					break;
				case 2:
					newX = x;
					newY = y - 1;
					break;
				case 3:
					newX = x;
					newY = y + 1;
					break;
				case 4:
					newX = x - 1;
					newY = y - 1;
					break;
				case 5:
					newX = x + 1;
					newY = y - 1;
					break;
				case 6:
					newX = x - 1;
					newY = y + 1;
					break;
				case 7:
					newX = x + 1;
					newY = y + 1;
					break;
				default:
					break;
			}
		}while (newX > 11 || newY < 0 || newX < 0 || newY > 11);
	}

	void move(int newX, int newY){
		myAnimals = Forest.myAnimals;
		forestNow = Forest.forestNow;
		if (newX == this.x && newY == this.y){
			System.out.println(this.name + " moved from " + this.x + "," + this.y + 
						" to " + this.x + "," + this.y);
				return;
		}
		if (forestNow[newX][newY].equals(".")){
			//just move
			forestNow[newX][newY] = this.label;
			forestNow[x][y] = ".";
			System.out.println(this.name + " moved from " + this.x + "," + this.y + 
						" to " + newX + "," + newY);
			x = newX;
			y = newY;
		}else{
			//fight
			Animal defender;
			int id=-1;
			if (forestNow[newX][newY].charAt(0) < 90){
				// dead body
				System.out.println(this.name + " moved from " + this.x + "," + this.y + 
						" to " + this.x + "," + this.y);
				return;
			}else{
				for (int i=0;i<8;i++){
					if (myAnimals[i].label.equals(forestNow[newX][newY])){
						// find the defender
						id = i;
					}
				}
				attack(myAnimals[id]);
			}
		}
	}
	boolean canWin(Animal defender){
		// If a Fox attacks a Cat, Fox wins and Cat dies.
		if (this.label.equals("f") && defender.label.equals("c")){
			return true;
		}

		// If a Feline attacks a Canine, Feline wins and Canine dies.
		if (this.label.equals("c") || this.label.equals("t") || this.label.equals("l")){
			if (defender.label.equals("d") || defender.label.equals("f") || defender.label.equals("w"))
				return true;
		}


		// If a Canine attacks a Feline, there is a 50% chance that one wins and the other dies.
		if (this.label.equals("d") || this.label.equals("w") || this.label.equals("f")){
			if (defender.label.equals("c") || defender.label.equals("t") || defender.label.equals("l")){
				return (rand.nextInt(2)==0);
			}
		}

		// If a Lion attacks a Hippo, Lion wins and Hippo dies.
		if (this.label.equals("l") && defender.label.equals("h")){
			return true;
		}

		// If any Animal attacks a Turtle, there is a 20% chance that the Animal wins and the Turtle dies.
		if (defender.label.equals("u")){
			return (rand.nextInt(5)==0);
		}
		// If a Turtle attacks an Animal, there is a 50% chance that the Turtle wins and the Animal dies.
		if (this.label.equals("u")){
			return (rand.nextInt(2)==0);
		}

		// In general attacker will lose and die
		return false;
	}

	void attack(Animal defender){
		forestNow[x][y] = ".";
		System.out.print(name + " from " + x + "," + y + " attacks " + defender.name + " at " + defender.x + "," + defender.y + " and ");
		if (this.canWin(defender)){
			// win
			do{
				defender.dieTo();
			}while(!forestNow[defender.newX][defender.newY].equals("."));

			defender.alive = false;
			defender.label = defender.label.toUpperCase();
			forestNow[newX][newY] = label;
			forestNow[defender.newX][defender.newY] = defender.label;
			defender.x = defender.newX;
			defender.y = defender.newY;
			System.out.print("wins.\n");
			System.out.println(defender.name + " dies at " + defender.x + "," + defender.y);
			System.out.println(name + " moved from " + x + "," + y + " to " + newX + "," + newY);
		}else{
			// lose
			System.out.print("loses.\n");
			x = newX;
			y = newY;
			do{
				this.dieTo();
			}while(!forestNow[this.newX][this.newY].equals("."));

			alive = false;
			label = label.toUpperCase();
			forestNow[newX][newY] = label;
			System.out.println(name + " dies at " + newX + "," + newY);
		}
		x = newX;
		y = newY;
	}
}
class Turtle extends Animal{
	Turtle(){
		name = "Turtle";
		label = "u";
	}
	void moveTo(){
		super.moveTo();
		if (rand.nextInt(2) == 1){
			newX = x;
			newY = y;
		}
	}
}
class Hippo extends Animal{
	Hippo(){
		name = "Hippo";
		label = "h";
	}
}
class Feline extends Animal{
	Feline(){
		name = "Feline";
	}
	void moveTo(){
		super.dieTo();
	}
}
class Canine extends Animal{
	Canine(){
		name = "Canine";
	}
	void moveTo(){
		super.moveTo();
	}
	void move(int newX, int newY){
		int oldX = x, oldY = y;
		if (rand.nextInt(2) == 0){
			// move 1 step
			super.move(this.newX,this.newY);
		}else{
			// move 2 step
			if (!forestNow[newX][newY].equals(".")){
				super.move(this.newX,this.newY);
				if (this.alive){
					this.newX = 2 * this.x - oldX;
					this.newY = 2 * this.y - oldY;
					if (this.newX > 11 || this.newY > 11 || this.newX < 0 || this.newY < 0) return;
					super.move(this.newX,this.newY);
				}
			}else{
				this.newX = 2 * this.newX - oldX;
				this.newY = 2 * this.newY - oldY;
				if (this.newX > 11 || this.newY > 11 || this.newX < 0 || this.newY < 0) return;
				super.move(this.newX,this.newY);
			}
			
		}

	}
	
}
class Tiger extends Feline{
	Tiger(){
		name = "Tiger";
		label = "t";
	}
}
class Cat extends Feline{
	Cat(){
		name = "Cat";
		label = "c";
	}
}class Lion extends Feline{
	Lion(){
		name = "Lion";
		label = "l";
	}
}
class Dog extends Canine{
	Dog(){
		name = "Dog";
		label = "d";
	}
}
class Wolf extends Canine{
	Wolf(){
		name = "Wolf";
		label = "w";
	}
}
class Fox extends Canine{
	Fox(){
		name = "Fox";
		label = "f";
	}
}
