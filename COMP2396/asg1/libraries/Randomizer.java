package libraries;

import java.util.ArrayList;
import java.util.Random;

/**
 * An abstraction of randomized result
 * @author andyyks
 */
public class Randomizer
{
  /**
   * Get a boolean value by a probability of true
   * @param probability A double-typed number between 0 and 1
   * @return True or False
   */
  public static boolean getBoolean(double probability)
  {
    if (!(0 <= probability && probability <= 1))
      return false;
    if (probability == 0)
      return false;
    if (probability == 1)
      return true;
    if (probability == 0.5)
      return (new Random()).nextBoolean();
    return new Random().nextDouble() < probability ? true : false;
  }

  /**
   * Draw an arbitrary element from ArrayList without replacement
   * @param inputArray ArrayList to be drawn
   * @return The element drawn
   */
  public static <T> T draw(ArrayList<T> inputArray)
  {
    int arr_size = inputArray.size();
    if (arr_size > 0)
    {
      int random_index = new Random().nextInt(arr_size);
      T result = inputArray.get(random_index);
      inputArray.remove(random_index);
      return result;
    }
    return null;
  }
}