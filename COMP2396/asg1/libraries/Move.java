package libraries;

/**
 * Represent a move of an animal in the forest
 * @author andyyks
 */
public class Move
{
  private Direction direction;
  private int maxStep;

  /**
   * Creates a new move
   * @param direction Direction to walk
   * @param maxStep Maximum allowed steps to walk
   */
  public Move(Direction direction, int maxStep)
  {
    this.direction = direction;
    this.maxStep = maxStep;
  }

  /**
   * Get the maximum allowed steps to walk
   * @return Number of steps
   */
  public int getMaxStep()
  {
    return this.maxStep;
  }

  /**
   * Get the target location on its direction within maximum allowed steps but not actually move
   * @param start Location from
   * @param step Steps to walk
   * @return Target location
   */
  public Location seek(Location start, int step)
  {
    return start.adjacency(this.direction, Math.min(step, this.maxStep));
  }
}