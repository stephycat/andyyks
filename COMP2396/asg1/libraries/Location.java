package libraries;

import java.util.LinkedHashMap;

/**
 * Represents a location in the forest
 * @author andyyks
 */
public class Location
{
  private LinkedHashMap<VComponent, Integer> coordinate;

  /**
   * Creates a new location
   * @param y Row index
   * @param x Column index
   */
  @SuppressWarnings("serial")
  public Location(int y, int x)
  {
    coordinate = new LinkedHashMap<VComponent, Integer>()
    {
      {
        put(VComponent.Y, new Integer(y));
        put(VComponent.X, new Integer(x));
      }
    };
  }

  /**
   * Get the index of specified component
   * @param c Vector component
   * @return The index of specified component
   */
  public int get(VComponent c)
  {
    return this.coordinate.get(c);
  }

  /**
   * Get the adjacent location based on current location
   * @param direction Direction to walk
   * @param step Steps to walk
   * @return Corresponding adjacent location
   */
  public Location adjacency(Direction direction, int step)
  {
    return new Location(
      this.get(VComponent.Y) + direction.get(VComponent.Y) * step,
      this.get(VComponent.X) + direction.get(VComponent.X) * step
    );
  }

  @Override
  public String toString()
  {
    return get(VComponent.Y) + "," + get(VComponent.X);
  }
}