import java.util.Scanner;
import java.util.Random;

public class Forest{
	public static Animal[] myAnimals;
	public static String[][] forestNow;
	public static void main(String[] args) {
		Scanner keyboard = new Scanner(System.in);
		String line;
		forestNow = buildForest();
		while(true) {
			System.out.print("Press enter to iterate, type 'exit' to quit: ");
			line = keyboard.nextLine();
			if (line.equals("exit")) {
				for(int i=0;i<8;i++){
					// print the list after typing "exit"
					if (myAnimals[i].alive)
						System.out.println(myAnimals[i].name + " is alive at " + myAnimals[i].x + "," + myAnimals[i].y);
					else
						System.out.println(myAnimals[i].name + " is dead at " + myAnimals[i].x + "," + myAnimals[i].y);
				}
				break;
			}
			// do simulation
			for(int i=0;i<8;i++){
				if (myAnimals[i].alive){
					myAnimals[i].moveTo();
					myAnimals[i].move(myAnimals[i].newX,myAnimals[i].newY);
				}
			}
			// you can print the forest if you like
			// printForest();
		}
	}
	private static String[][] buildForest(){
		String[][] ans = new String[12][12];
		myAnimals = new Animal[8];
		int i,j;
		for(i=0;i<12;i++){
			for(j=0;j<12;j++){
				ans[i][j] = ".";
			}
		}
		myAnimals[0] = new Cat();
		myAnimals[1] = new Dog();
		myAnimals[2] = new Fox();
		myAnimals[3] = new Hippo();
		myAnimals[4] = new Lion();
		myAnimals[5] = new Tiger();
		myAnimals[6] = new Turtle();
		myAnimals[7] = new Wolf();
		for (i=0;i<8;i++){
			Random ran = new Random();
			int row,column;
			do{
				row = ran.nextInt(12);
				column = ran.nextInt(12);
			}while(!ans[row][column].equals("."));
			myAnimals[i].x = row;
			myAnimals[i].y = column;
			ans[row][column] = myAnimals[i].label;
		}
		for(i=0;i<12;i++){
			for(j=0;j<12;j++){
				System.out.print(ans[i][j]);
			}
			System.out.print("\n");
		}
		return ans;
	}
	private static void printForest(){
		// you can print the forest if you like
		for(int i=0;i<12;i++){
			for(int j=0;j<12;j++){
				System.out.print(forestNow[i][j]);
			}
			System.out.println(" ");
		}
	}

}
