class Canine extends Animal{
	Canine(){
		name = "Canine";
	}
	void moveTo(){
		super.moveTo();
	}
	void move(int newX, int newY){
		int oldX = x, oldY = y;
		if (rand.nextInt(2) == 0){
			// move 1 step
			super.move(this.newX,this.newY);
		}else{
			// move 2 step
			if (!forestNow[newX][newY].equals(".")){
				super.move(this.newX,this.newY);
				if (this.alive){
					this.newX = 2 * this.x - oldX;
					this.newY = 2 * this.y - oldY;
					if (this.newX > 11 || this.newY > 11 || this.newX < 0 || this.newY < 0) return;
					super.move(this.newX,this.newY);
				}
			}else{
				this.newX = 2 * this.newX - oldX;
				this.newY = 2 * this.newY - oldY;
				if (this.newX > 11 || this.newY > 11 || this.newX < 0 || this.newY < 0) return;
				super.move(this.newX,this.newY);
			}
			
		}

	}
	
}