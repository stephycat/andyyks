package animals;

import java.util.ArrayList;
import static java.util.Arrays.asList;
import game.*;
import libraries.*;

/**
 * Super class for animals in the forest
 * @author andyyks
 */
public abstract class Animal
{
  protected String label;
  protected Forest forest;
  protected Location location;
  protected ArrayList<Move> defaultMoves;
  protected boolean isAlive = true;

  /**
   * Creates an animal with label and specified forest 
   * @param label Symbol to be displayed in forest
   * @param forest The forest which the animal in
   */
  public Animal(String label, Forest forest)
  {
    this.label = label;
    this.forest = forest;
    this.location = null;
    this.defaultMoves = new ArrayList<> (
      asList(
        new Move(Direction.NORTH, 1),
        new Move(Direction.EAST, 1),
        new Move(Direction.SOUTH, 1),
        new Move(Direction.WEST, 1)
      )
    );
  }

  /**
   * Get the symbol to be displayed in forest
   * @return This animal's symbol
   */
  public String getLabel()
  {
    return (this.isAlive ? this.label : this.label.toUpperCase());
  }

  /**
   * Get the animal's location in forest
   * @return This animal's location in forest
   */
  public Location getLocation()
  {
    return this.location;
  }

  /**
   * Set the animal's location in forest
   * @param location Target location in forest
   * @return Target location in forest
   */
  public Location setLocation(Location location)
  {
    this.location = location;
    return location;
  }

  /**
   * Attacks an animal
   * @param animal Attackee
   * @return True for successful attack and live,
   *         False for failed attack and dead
   */
  public boolean attack(Animal animal)
  {
    if (animal instanceof Turtle)
      if (!animal.die(0.2))
        return !this.die();
      else
        return true;
    else
      return !this.die();
  }

  /**
   * Make this animal to dead
   * @return Must be true
   */
  public boolean die()
  {
    return this.die(1.0);
  }

  /**
   * Make this animal to dead by a probability
   * @param probability A double-typed number between 0 and 1
   * @return True for this animal is dead,
   *         False for this animal is still alive
   */
  public boolean die(final double probability)
  {
    if (this.isAlive && Randomizer.getBoolean(probability))
      this.isAlive = false;
    return !this.isAlive;
  }

  /**
   * Check whether this animal is alive
   * @return True for this animal is alive,
   *         False for this animal is dead
   */
  public boolean isAlive()
  {
    return this.isAlive;
  }

  /**
   * Make a valid move in a cycle
   */
  public void moveInTurn()
  {
    ArrayList<Move> moves = new ArrayList<Move>();
    for (Move move: this.defaultMoves)
    {
      Location target_location = move.seek(this.location, move.getMaxStep());
      if (this.forest.isInside(target_location))
        moves.add(move);
    }
    
    Move selected_move;
    if (this instanceof Turtle && !Randomizer.getBoolean(0.5))
      selected_move = null;
    else
      selected_move = Randomizer.draw(moves);

    if (selected_move != null)
    {
      Location self_original_location = this.location;
      for (int s=1; s<=selected_move.getMaxStep(); s++)
      {
        Location self_former_location = this.location;
        Location self_target_location = selected_move.seek(this.location, 1);

        if (this.forest.isEmpty(self_target_location))
        {
          // System.out.println("Move without attack");
          this.forest.setAnimalAt(null, this.getLocation());
          this.forest.setAnimalAt(this, this.setLocation(self_target_location));
        }
        else
        {
          Animal animal = this.forest.getAnimalAt(self_target_location);
          if (animal.isAlive())
          {
            if (this.attack(animal))
            {
              // Attacker wins
              System.out.println(
                this.getClass().getSimpleName() + " from " + self_former_location + " attacks " +
                animal.getClass().getSimpleName() + " at " + self_target_location + " and wins"
              );
 
              Location opponent_former_location = animal.getLocation();
              if (animal.moveToNearby())
              {
                this.forest.setAnimalAt(null, opponent_former_location);
                this.forest.setAnimalAt(animal, animal.getLocation());

                this.forest.setAnimalAt(null, this.getLocation());
                this.forest.setAnimalAt(this, this.setLocation(self_target_location));
              }
              System.out.println(animal.getClass().getSimpleName() + " dies at " + animal.getLocation());
            }
            else
            {
              // Attacker loss
              System.out.println(
                this.getClass().getSimpleName() + " from " + self_former_location + " attacks " +
                animal.getClass().getSimpleName() + " at " + self_target_location + " and loses"
              );
 
              this.setLocation(self_target_location);
              if (this.moveToNearby())
              {
                this.forest.setAnimalAt(null, self_former_location);
                this.forest.setAnimalAt(this, this.getLocation());
              }
              else
                this.setLocation(self_former_location);

              System.out.println(this.getClass().getSimpleName() + " dies at " + this.getLocation());
              break;
            }
          }
        }
      }
      if (this.isAlive())
        System.out.println(
          this.getClass().getSimpleName() + " moved from " +
          self_original_location + " to " + this.getLocation()
        );
    }
    else
    {
      System.out.println(this.getClass().getSimpleName() + " moved from " + this.location + " to " + this.location);
    }
  }

  /**
   * Move to one of eight near-by empty locations
   * @return True for moved to another location,
   *         False for stayed in current location
   */
  public boolean moveToNearby()
  {
    ArrayList<Location> locations = new ArrayList<Location>();
    for (Direction direction: Direction.values())
    {
      Location self_target_location = this.location.adjacency(direction, 1);
      if (this.forest.isInside(self_target_location) && this.forest.isEmpty(self_target_location))
        locations.add(self_target_location);
    }
    Location selected_location = Randomizer.draw(locations);
    if (selected_location != null)
    {
      this.setLocation(selected_location);
      return true;
    }
    else
      return false;
  }
}