package animals;

import game.*;

/**
 * Super class for wolves in the forest
 * @author andyyks
 */
public class Wolf extends Canine
{
  /**
   * Creates a wolf with specified forest
   * @param forest The forest which the wolf in
   */
  public Wolf(Forest forest)
  {
    super("w", forest);
  }
}