package animals;

import game.*;

/**
 * Super class for lions in the forest
 * @author andyyks
 */
public class Lion extends Feline
{
  /**
   * Creates a lion with specified forest
   * @param forest The forest which the lion in
   */
  public Lion(Forest forest)
  {
    super("l", forest);
  }

  @Override
  public boolean attack(Animal animal)
  {
    if (animal instanceof Hippo)
      return animal.die();
    else
      return super.attack(animal);
  }
}