package animals;

import game.*;

/**
 * Super class for cats in the forest
 * @author andyyks
 */
public class Cat extends Feline
{
  /**
   * Creates a cat with specified forest
   * @param forest The forest which the cat in
   */
  public Cat(Forest forest)
  {
    super("c", forest);
  }
}