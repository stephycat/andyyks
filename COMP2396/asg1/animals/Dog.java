package animals;

import game.*;

/**
 * Super class for dogs in the forest
 * @author andyyks
 */
public class Dog extends Canine
{
  /**
   * Creates a dog with specified forest
   * @param forest The forest which the dog in
   */
  public Dog(Forest forest)
  {
    super("d", forest);
  }
}