package animals;

import game.*;

/**
 * Super class for turtles in the forest
 * @author andyyks
 */
public class Turtle extends Animal
{
  /**
   * Creates a turtle with specified forest
   * @param forest The forest which the turtle in
   */
  public Turtle(Forest forest)
  {
    super("u", forest);
  }

  @Override
  public boolean attack(Animal animal)
  {
    if (!animal.die(0.5))
      return !this.die();
    else
      return true;
  }
}