package animals;

import static java.util.Arrays.asList;
import game.*;
import libraries.*;

/**
 * Super class for felines in the forest
 * @author andyyks
 */
public abstract class Feline extends Animal
{
  /**
   * Creates a feline with label and specified forest 
   * @param label Symbol to be displayed in forest
   * @param forest The forest which the feline in
   */
  public Feline(String label, Forest forest)
  {
    super(label, forest);
    this.defaultMoves.addAll(
      asList(
        new Move(Direction.NORTH_EAST, 1),
        new Move(Direction.SOUTH_EAST, 1),
        new Move(Direction.SOUTH_WEST, 1),
        new Move(Direction.NORTH_WEST, 1)
      )
    );
  }

  @Override
  public boolean attack(Animal animal)
  {
    if (animal instanceof Canine)
      return animal.die();
    else
      return super.attack(animal);
  }
}