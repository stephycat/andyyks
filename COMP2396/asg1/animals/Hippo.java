package animals;

import game.*;

/**
 * Super class for hippos in the forest
 * @author andyyks
 */
public class Hippo extends Animal
{
  /**
   * Creates a hippo with specified forest
   * @param forest The forest which the hippo in
   */
  public Hippo(Forest forest)
  {
    super("h", forest);
  }
}