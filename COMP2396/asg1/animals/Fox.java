package animals;

import game.*;

/**
 * Super class for foxes in the forest
 * @author andyyks
 */
public class Fox extends Canine
{
  /**
   * Creates a fox with specified forest
   * @param forest The forest which the fox in
   */
  public Fox(Forest forest)
  {
    super("f", forest);
  }

  @Override
  public boolean attack(Animal animal)
  {
    if (animal instanceof Cat)
      return animal.die();
    else
      return super.attack(animal);
  }
}