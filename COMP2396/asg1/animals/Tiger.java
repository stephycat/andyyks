package animals;

import game.*;

/**
 * Super class for tigers in the forest
 * @author andyyks
 */
public class Tiger extends Feline
{
  /**
   * Creates a tiger with specified forest
   * @param forest The forest which the tiger in
   */
  public Tiger(Forest forest)
  {
    super("t", forest);
  }
}