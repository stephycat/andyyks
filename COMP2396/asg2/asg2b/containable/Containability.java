package containable;

/**
 * @author andyyks
 */
public class Containability
{
  private int multiplier;
  private int addend;

  /**
   * @param multiplier The multiple of relative capacity
   * @param addend The constant capacity
   */
  public Containability(int multiplier, int addend)
  {
    this.multiplier = multiplier;
    this.addend = addend;
  }

  /**
   * Get the capacity based on multiplier and addend
   * @param volume Volume to be referenced
   * @return multiplier * volume + addend
   */
  public int getCapacity(int volume)
  {
    return volume * this.multiplier + this.addend;
  }

  /*
  @Override
  public boolean equals(Object o)
  {
    if (o == this)
      return true;

    if (!(o instanceof Containability))
      return false;

    Containability c = (Containability) o;
    return Integer.compare(multiplier, c.multiplier)==0 && Integer.compare(addend, c.addend)==0;
  }
  */
}