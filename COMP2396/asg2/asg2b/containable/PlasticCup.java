package containable;

/**
 * A static container - Plastic Cup
 * @author andyyks
 */
public class PlasticCup implements Container
{
  public int getCapacity(Containability containability)
  {
    return containability.getCapacity(0);
  }
}