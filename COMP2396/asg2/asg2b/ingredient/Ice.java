package ingredient;

/**
 * Ice indregient
 * @author andyyks
 */
public class Ice extends Ingredient
{
  /**
   * Add ice as ingredient
   * @param minVolume
   * @param maxVolume
   */
  public Ice(int minVolume, int maxVolume)
  {
    super(minVolume, maxVolume);
  }
}
