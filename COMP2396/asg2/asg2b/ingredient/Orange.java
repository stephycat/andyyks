package ingredient;

/**
 * Orange ingredient
 * @author andyyks
 */
public class Orange extends Ingredient
{
  /**
   * Add orange as ingredient
   * @param minVolume
   * @param maxVolume
   */
  public Orange(int minVolume, int maxVolume)
  {
    super(minVolume, maxVolume);
  }
}
