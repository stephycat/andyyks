package ingredient;

/**
 * Coconut ingredient
 * @author andyyks
 */
public class Coconut extends Ingredient
{
  /**
   * Add coconut as ingredient
   * @param minVolume
   * @param maxVolume
   */
  public Coconut(int minVolume, int maxVolume)
  {
    super(minVolume, maxVolume);
  }
}
