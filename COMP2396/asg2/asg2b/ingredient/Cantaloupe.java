package ingredient;

/**
 * Cantaloupe ingredient
 * @author andyyks
 */
public class Cantaloupe extends Ingredient 
{
  /**
   * Add cantaloupe as ingredient
   * @param minVolume
   * @param maxVolume
   */
  public Cantaloupe (int minVolume, int maxVolume)
  {
    super(minVolume, maxVolume);
  }
}
