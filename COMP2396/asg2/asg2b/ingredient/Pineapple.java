package ingredient;

/**
 * Pineapple ingredient
 * @author andyyks
 */
public class Pineapple extends Ingredient 
{
  /**
   * Add pineapple as ingredient
   * @param minVolume
   * @param maxVolume
   */
  public Pineapple(int minVolume, int maxVolume)
  {
    super(minVolume, maxVolume);
  }
}
