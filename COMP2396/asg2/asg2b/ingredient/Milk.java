package ingredient;

/**
 * Milk ingredient
 * @author andyyks
 */
public class Milk extends Ingredient 
{
  /**
   * Add milk as ingredient
   * @param minVolume
   * @param maxVolume
   */
  public Milk(int minVolume, int maxVolume)
  {
    super(minVolume, maxVolume);
  }
}
