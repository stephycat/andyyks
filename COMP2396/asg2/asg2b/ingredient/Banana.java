package ingredient;

/**
 * Banana ingredient
 * @author andyyks
 */
public class Banana extends Ingredient
{
  /**
   * Add banana as ingredient
   * @param minVolume
   * @param maxVolume
   */
  public Banana(int minVolume, int maxVolume)
  {
    super(minVolume, maxVolume);
  }
}
