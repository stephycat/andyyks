package ingredient;

/**
 * Peach ingredient
 * @author andyyks
 */
public class Peach extends Ingredient 
{
  /**
   * Add peach as ingredient
   * @param minVolume
   * @param maxVolume
   */
  public Peach(int minVolume, int maxVolume)
  {
    super(minVolume, maxVolume);
  }
}
