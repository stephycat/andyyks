package ingredient;

/**
 * Chocolate chips ingredient
 * @author andyyks
 */
public class ChocolateChips extends Ingredient
{
  /**
   * Add chocolate chips as ingredient
   * @param minVolume
   * @param maxVolume
   */
  public ChocolateChips(int minVolume, int maxVolume)
  {
    super(minVolume, maxVolume);
  }
}
