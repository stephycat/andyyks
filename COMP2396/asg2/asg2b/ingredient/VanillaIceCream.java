package ingredient;

/**
 * Vanilla ice-cream ingredient
 * @author andyyks
 */
public class VanillaIceCream extends Ingredient
{
  /**
   * Add vanilla ice-cream as ingredient
   * @param minVolume
   * @param maxVolume
   */
  public VanillaIceCream(int minVolume, int maxVolume)
  {
    super(minVolume, maxVolume);
  }
}
