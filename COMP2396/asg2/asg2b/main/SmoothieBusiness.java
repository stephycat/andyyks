package main;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.LinkedHashMap;
import java.util.Scanner;
import containable.*;
import ingredient.*;

/**
 * Represents the Smoothie Business
 * @author andyyks
 */
public class SmoothieBusiness
{
  private ArrayList<SimpleEntry<Class, Integer[]>> selectable_ingredients;
  private ArrayList<Ingredient> selected_ingredients;

  private LinkedHashMap<Class, Containability> available_containers;
  private ArrayList<SimpleEntry<Container, Containability>> selectable_containers;
  private Container selected_container;

  /**
   * Create a new Smoothie Business
   */
  public SmoothieBusiness()
  {
    this.selectable_ingredients = new ArrayList<SimpleEntry<Class, Integer[]>>();
    this.selected_ingredients = new ArrayList<Ingredient>();
    this.available_containers = new LinkedHashMap<Class, Containability>();
    this.selectable_containers = new ArrayList<SimpleEntry<Container, Containability>>();
  }

  /**
   * Introduce an ingredient to the business
   * @param className The class name which extends Ingredient class
   * @param volumeRange new Integer[] { minVolume, maxVolume }
   * @return true for success, false for failed
   */
  public boolean regIngredient(String className, Integer[] volumeRange)
  {
    Class ingredient = Ingredient.specify(className);
    if (ingredient != null)
    {
      this.selectable_ingredients.add(new SimpleEntry<Class, Integer[]>(ingredient, volumeRange));
      return true;
    }
    else
      return false;
  }

  /**
   * Introduce a container to the business
   * @param className The class name which implements Container interface
   * @param containability
   * @return true for success, false for failed
   */
  @SuppressWarnings("unchecked")
  public boolean regContainer(String className, Containability containability)
  {
    Class container = Container.specify(className);
    if (container != null)
    {
      if (this.available_containers.containsKey(container))
        return false;

      this.available_containers.put(container, containability);
      if (container.getPackage().getName().equals("containable"))
      {
        // for (SimpleEntry<Container, Containability> selectable_container: this.selectable_containers)
        // {
          // String current_className = selectable_container.getKey().getClass().getCanonicalName();
          // Containability current_containability = selectable_container.getValue();
          // if (className.equals(current_className) && containability.equals(current_containability))
            // return false;
        // }

        try {
          Container static_container = (Container) container.getConstructor().newInstance();
          this.selectable_containers.add(new SimpleEntry<Container, Containability>(static_container, containability));
        }
        catch (IllegalAccessException | InstantiationException | InvocationTargetException | NoSuchMethodException e) {
          e.printStackTrace();
          return false;
        }
      }
      return true;
    }
    else
      return false;
  }

  /**
   * Remove an ingredient from the business
   * @param index The order number that the ingredient was registered
   * @return true for success, false for failed
   */
  public boolean deregIngredient(int index)
  {
    try {
      this.selectable_ingredients.remove(index);
      return true;
    }
    catch (IndexOutOfBoundsException e) {
      return false;
    }
  }

  /*
  public boolean deregContainer(int index)
  {
    try {
      this.available_containers.remove(index);
      return true;
    }
    catch (IndexOutOfBoundsException e) {
      return false;
    }
  }
  */

  /**
   * Run the business
   */
  @SuppressWarnings("unchecked")
  public void run()
  {
    int ingredient_choice, container_choice, total_ingredient_volume=0;

    if (!this.isExistStaticContainers())
    {
      System.out.println("Cannot launch the system as no static container.");
      return;
    }

    do {
      this.displayIngredientsMenu();
      ingredient_choice = this.getIntegerInput("Please enter your choice (1-%d, or 0 to finish the order): ", 0, this.selectable_ingredients.size());
      try
      {
        if (ingredient_choice > 0)
        {
          SimpleEntry<Class, Integer[]> selected_ingredient = this.selectable_ingredients.get(ingredient_choice-1);
          Constructor<?> c = selected_ingredient.getKey().getConstructor(int.class, int.class);
          Integer[] argv = selected_ingredient.getValue();
          Ingredient ingredient = (Ingredient) c.newInstance(argv[0], argv[1]);

          System.out.printf("\nAdded %s %dml\n\n", this.formatClassName(ingredient.getClass().getSimpleName()), ingredient.getVolume());
          selected_ingredients.add(ingredient);
          total_ingredient_volume += ingredient.getVolume();

          if (this.available_containers.containsKey(ingredient.getClass()))
          {
            Containability containability = this.available_containers.get(ingredient.getClass());
            this.selectable_containers.add(new SimpleEntry<Container, Containability>(ingredient, containability));
          }
        }
      }
      catch (IllegalAccessException | InstantiationException | InvocationTargetException | NoSuchMethodException e) {
        e.printStackTrace();
      }
    } while (ingredient_choice > 0);

    if (this.selected_ingredients.size() > 0)
    {
      SimpleEntry<Container, Containability> selected_container;
      if (this.selectable_containers.size() > 1)
      {
        System.out.println();
        this.displayContainersMenu();
        container_choice = this.getIntegerInput("Please enter your choice (1-%d): ", 1, this.selectable_containers.size());
        selected_container = this.selectable_containers.get(container_choice-1);
      }
      else
        selected_container = this.selectable_containers.get(0);

      String container_name = this.formatClassName(selected_container.getKey().getClass().getSimpleName());
      int container_capacity = selected_container.getKey().getCapacity(selected_container.getValue());
      int container_vs_ingredient = container_capacity - total_ingredient_volume;

      // Process Ice
      if (container_vs_ingredient > 0)
      {
        Ice ice = new Ice(container_vs_ingredient, container_vs_ingredient);
        selected_ingredients.add(ice);
        total_ingredient_volume += container_vs_ingredient;
        System.out.printf("\nAdded %s %dml\n", this.formatClassName(ice.getClass().getSimpleName()), container_vs_ingredient);
      }

      this.displaySmoothieInfo(container_name, container_capacity, -container_vs_ingredient);
    }
  }

  private String rightAlignNumber(int number, int related_max)
  {
    int longest_digits = (int) Math.floor(Math.log10(related_max)) + 1;
    return String.format("%"+longest_digits+"s", number);
  }

  private boolean isExistStaticContainers()
  {
    for (SimpleEntry<Container, Containability> selectable_container: this.selectable_containers)
      if (selectable_container.getKey().getClass().getPackage().getName().equals("containable"))
        return true;
    return false;
  }

  private void displayIngredientsMenu()
  {
    int list_size = this.selectable_ingredients.size();
    for (int i=0; i<list_size; i++)
      System.out.printf("%s. %s\n", this.rightAlignNumber(i+1, list_size), this.formatClassName(this.selectable_ingredients.get(i).getKey().getSimpleName()));
    System.out.println("What would you like to add to your smoothie?");
  }

  private void displayContainersMenu()
  {
    int list_size = this.selectable_containers.size();
    for (int i=0; i<list_size; i++)
    {
      SimpleEntry<Container, Containability> selectable_container = this.selectable_containers.get(i);
      String container_name = this.formatClassName(selectable_container.getKey().getClass().getSimpleName());
      int container_capacity = selectable_container.getKey().getCapacity(selectable_container.getValue());
      System.out.printf("%s. %s (%dml)\n", this.rightAlignNumber(i+1, list_size), container_name, container_capacity);
    }
    System.out.println("What would you like to use to hold your smoothie?");
  }

  private void displaySmoothieInfo(String container_name, int container_capacity, int excess_volume)
  {
    System.out.println("\nSmoothie ingredients:");
    for (Ingredient ingredient: this.selected_ingredients)
    {
      String ingredient_name = this.formatClassName(ingredient.getClass().getSimpleName());
      System.out.printf("%s (%dml)\n", ingredient_name, ingredient.getVolume());
    }
    System.out.printf("Container: %s (%dml)\n", container_name, container_capacity);
    if (excess_volume > 0)
      System.out.printf("Wasted %dml!\n", excess_volume);
  }

  private String formatClassName(String className)
  {
    String lCaseName = String.join(" ", className.split("(?=[A-Z])")).toLowerCase();
    return lCaseName.substring(0, 1).toUpperCase() + lCaseName.substring(1);
  }

  private int getIntegerInput(String prompt, int min, int max)
  {
    Scanner scanner = new Scanner(System.in);
    int input = -1;
    do {
      System.out.printf(prompt, max);
      try { input = scanner.nextInt(); }
      catch (InputMismatchException e) { scanner.next(); }
    } while (!(min<=input && input<=max));
    return input;
  }
}
