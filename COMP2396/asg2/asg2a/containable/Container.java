package containable;

/**
 * Represents a container
 * @author andyyks
 */
public interface Container
{
  /**
   * Get capacity based on containability
   * @param containability
   * @return Container capacity
   */
  int getCapacity(Containability containability);

  /**
   * Get a specific container class
   * @param className The class name which implements Container interface
   * @return The container class
   */
  static Class specify(String className)
  {
    try
    {
      Class targetClass = Class.forName(className);
      if (Container.class.isAssignableFrom(targetClass))
        return targetClass;
      else
        return null;
    } 
    catch (ClassNotFoundException e)
    {
      return null;
    }
  }
}