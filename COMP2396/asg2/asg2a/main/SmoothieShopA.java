package main;

import java.util.*;
import containable.*;
import ingredient.*;

/**
 * The main program that controls the program flow and user interaction
 * @author andyyks
 */
public class SmoothieShopA
{
  public static void main (String[] args)
  {
    SmoothieBusiness business = new SmoothieBusiness();

    business.regIngredient("Apple", new Integer[] {40, 50});
    business.regIngredient("Orange", new Integer[] {50, 60});
    business.regIngredient("Banana", new Integer[] {35, 40});
    business.regIngredient("Melon", new Integer[] {50, 70});
    business.regIngredient("WaterMelon", new Integer[] {150, 150});
    business.regIngredient("Coconut", new Integer[] {60, 80});
    business.regIngredient("ChocolateChips", new Integer[] {30, 40});
    business.regIngredient("Milk", new Integer[] {100, 100});
    business.regIngredient("VanillaIceCream", new Integer[] {40, 45});

    business.regContainer("containable.PlasticCup", new Containability(0, 300));
    business.regContainer("ingredient.Melon", new Containability(4, 0));
    business.regContainer("ingredient.Coconut", new Containability(5, 0));
    business.regContainer("ingredient.WaterMelon", new Containability(0, 350));

    business.run();
    System.exit(0);
  }
}
