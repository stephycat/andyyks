package ingredient;

/**
 * Apple ingredient
 * @author andyyks
 */
public class Apple extends Ingredient
{
  /**
   * Add apple as ingredient
   * @param minVolume
   * @param maxVolume
   */
  public Apple(int minVolume, int maxVolume)
  {
    super(minVolume, maxVolume);
  }
}
