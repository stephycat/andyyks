package ingredient;

/**
 * Watermelon ingredient
 * @author andyyks
 */
public class WaterMelon extends Ingredient
{
  /**
   * Add watermelon as ingredient
   * @param minVolume
   * @param maxVolume
   */
  public WaterMelon(int minVolume, int maxVolume)
  {
    super(minVolume, maxVolume);
  }
}
