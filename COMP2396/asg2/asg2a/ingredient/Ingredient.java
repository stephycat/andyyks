package ingredient;

import java.util.Random;
import containable.*;

/**
 * Super class of ingredients
 * @author andyyks
 */
public abstract class Ingredient implements Container
{
  protected int curVolume;

  /**
   * Add new ingredient
   * @param minVolume
   * @param maxVolume
   */
  public Ingredient(int minVolume, int maxVolume)
  {
    this.curVolume = new Random().nextInt((maxVolume - minVolume) + 1) + minVolume;
  }

  /**
   * Get a specific Ingredient class
   * @param className The class name which extends Ingredient class
   * @return The ingredient class
   */
  public static Class specify(String className)
  {
    try
    {
      Class targetClass = Class.forName("ingredient." + className);
      if (Ingredient.class.isAssignableFrom(targetClass))
        return targetClass;
      else
        return null;
    } 
    catch (ClassNotFoundException e)
    {
      return null;
    }
  }

  /**
   * @return Current ingredient volume
   */
  public int getVolume()
  {
    return this.curVolume;
  }

  public int getCapacity(Containability containability)
  {
    return containability.getCapacity(this.curVolume);
  }
}
