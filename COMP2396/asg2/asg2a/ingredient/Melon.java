package ingredient;

/**
 * Melon ingredient
 * @author andyyks
 */
public class Melon extends Ingredient
{
  /**
   * Add melon as ingredient
   * @param minVolume
   * @param maxVolume
   */
  public Melon(int minVolume, int maxVolume)
  {
    super(minVolume, maxVolume);
  }
}
