#!/bin/bash

clear
cd asg2a
javac -Xlint:unchecked **/*.java
java main/SmoothieShopA
find . -type f -name '*.class' -exec rm {} + 
