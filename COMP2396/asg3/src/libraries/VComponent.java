package libraries;

/**
 * Define the two vector component in a 2-dimension plane
 * @author andyyks
 */
public enum VComponent
{
  X, Y;
}