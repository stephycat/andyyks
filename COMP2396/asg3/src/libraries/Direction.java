package libraries;

import java.util.LinkedHashMap;

/**
 * Defines eight moving directions
 * @author andyyks
 */
public enum Direction
{
  NORTH     (-1,  0),
  NORTH_EAST(-1,  1),
  EAST      ( 0,  1),
  SOUTH_EAST( 1,  1),
  SOUTH     ( 1,  0),
  SOUTH_WEST( 1, -1),
  WEST      ( 0, -1),
  NORTH_WEST(-1, -1);

  private final LinkedHashMap<VComponent, Integer> offset;

  /**
   * Constructor of Direction
   * @param y Row index
   * @param x Column index
   */
  @SuppressWarnings("serial")
  Direction(final int y, final int x)
  {
    this.offset = new LinkedHashMap<VComponent, Integer>()
    {
      {
        put(VComponent.Y, new Integer(y));
        put(VComponent.X, new Integer(x));
      }
    };
  }

  /**
   * Get the index of specified component
   * @param c Vector component
   * @return The index of specified component
   */
  public int get(VComponent c)
  {
    return this.offset.get(c);
  }

  @Override
  public String toString()
  {
    return "[" + get(VComponent.Y) + ", " + get(VComponent.X) + "]";
  }
}