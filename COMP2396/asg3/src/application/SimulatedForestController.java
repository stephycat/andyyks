package application;

import animals.Animal;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.stream.Collectors;
import java.util.AbstractMap.SimpleEntry;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.HPos;
import javafx.geometry.VPos;
import javafx.scene.Node;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.RowConstraints;
import libraries.*;

/**
 * Control the GUI Forest
 * @author andyyks
 */
public class SimulatedForestController
{
  private enum States { IDLE, ANIMAL_SELECTED }
  private States state;
  private Forest actual_forest;
  private Animal subject_animal;
  private LinkedHashMap<Location, Move> movable_locations;
  private int alive_animals;
  private int total_animals;
  private String cached_class = null;

  @FXML
  private GridPane grid_forest;
  @FXML
  private Button btn_auto_mode;

  /**
   * For receiving data from start menu
   * @param forest The forest in memory
   * @param num_of_animals Total number of registered animals
   */
  public SimulatedForestController(Forest forest, int num_of_animals)
  {
    this.actual_forest = forest;
    this.actual_forest.init();
    this.state = States.IDLE;
    this.alive_animals = num_of_animals;
    this.total_animals = num_of_animals;
  }

  @FXML
  private void initialize()
  {
    this.grid_forest.setPrefSize(AnimalGUI.NUM_COLS * (AnimalGUI.ICON_WIDTH+2), AnimalGUI.NUM_ROWS * (AnimalGUI.ICON_HEIGHT+2));
    for (int i = 0; i < AnimalGUI.NUM_COLS; i++)
    {
      ColumnConstraints col_const = new ColumnConstraints();
      col_const.setPercentWidth(100.0 / AnimalGUI.NUM_COLS);
      this.grid_forest.getColumnConstraints().add(col_const);
    }
    for (int i = 0; i < AnimalGUI.NUM_ROWS; i++)
    {
      RowConstraints row_const = new RowConstraints();
      row_const.setPercentHeight(100.0 / AnimalGUI.NUM_ROWS);
      this.grid_forest.getRowConstraints().add(row_const);
    }
    for (int c = 0; c < AnimalGUI.NUM_COLS; c++)
    {
      for (int r = 0; r < AnimalGUI.NUM_ROWS; r++)
      {
        Pane cell_pane = new Pane();
        cell_pane.addEventFilter(MouseEvent.MOUSE_CLICKED, handleSelectCell);
        cell_pane.addEventFilter(MouseEvent.MOUSE_ENTERED, handleHoverCell);
        cell_pane.addEventFilter(MouseEvent.MOUSE_EXITED, handleHoverCell);
        cell_pane.getStyleClass().add("grid_forest_cell");
        cell_pane.setMaxWidth(AnimalGUI.ICON_WIDTH + 2);
        cell_pane.setMaxHeight(AnimalGUI.ICON_HEIGHT + 2);
        if (c == 0) cell_pane.getStyleClass().add("first-column");
        if (r == 0) cell_pane.getStyleClass().add("first-row");
        this.grid_forest.add(cell_pane, c, r);
        GridPane.setHalignment(cell_pane, HPos.CENTER);
        GridPane.setValignment(cell_pane, VPos.CENTER);
      }
    }
    for (Animal animal: this.actual_forest.getAnimals())
      this.setCell(animal.getLocation(), animal);
  }

  @FXML
  private void performAutoMode(ActionEvent event)
  {
    MouseEvent mouse_clicked_event = new MouseEvent(MouseEvent.MOUSE_CLICKED,
        0, 0, 0, 0, MouseButton.PRIMARY, 1, false, false, false,
        false, false, false, false, false, true, false, null);
    for (Animal animal: this.actual_forest.getAnimals())
    {
      if (animal.isAlive())
      {
        this.getCell(animal.getLocation()).fireEvent(mouse_clicked_event);
        this.getCell(Randomizer.draw(new ArrayList<>(this.movable_locations.keySet()))).fireEvent(mouse_clicked_event);
      }
    }
  }

  private Pane getCell(Location location)
  {
    int row = location.get(VComponent.Y);
    int col = location.get(VComponent.X);
    for (Node node : this.grid_forest.getChildren())
      if (node instanceof Pane && GridPane.getColumnIndex(node) == col && GridPane.getRowIndex(node) == row)
        return (Pane) node;
    return null;
  }

  private void setCell(Location location, Animal animal)
  {
    ImageView iv = animal.getIcon();
    iv.setFitWidth(AnimalGUI.ICON_WIDTH * (animal.isAlive() ? 1 : 0.75));
    iv.setFitHeight(AnimalGUI.ICON_HEIGHT * (animal.isAlive() ? 1 : 0.75));
    this.getCell(location).getChildren().add(iv);
    animal.setLocation(location);
  }
  
  private void highlightMovableCells(LinkedHashMap<Location, Move> moves, boolean isHighlight)
  {
    Pane cell;
    for (Location location: moves.keySet())
    {
      cell = this.getCell(location);
      ObservableList<String> cell_style_classes = cell.getStyleClass();
      for (Node node: cell.getChildren())
        node.setVisible(!isHighlight);
      if (isHighlight)
      {
        if (this.actual_forest.isEmpty(location))
          cell_style_classes.add("movable_location");
        else
          cell_style_classes.add("attackable_location");
      }
      else
      {
        cell_style_classes.remove("hovered_location");
        cell_style_classes.remove("movable_location");
        cell_style_classes.remove("attackable_location");
      }
    }
  }

  private void resetToIdle()
  {
    this.highlightMovableCells(this.movable_locations, false);
    this.subject_animal = null;
    this.movable_locations.clear();
    this.state = States.IDLE;
  }
  
  private EventHandler<MouseEvent> handleSelectCell = event -> {
    switch (this.state)
    {
      case IDLE:
        this.subject_animal = this.actual_forest.getAnimalAt(new Location(
            GridPane.getRowIndex((Pane) event.getSource()), GridPane.getColumnIndex((Pane) event.getSource())));
        if (this.subject_animal != null && this.subject_animal.isAlive())
        {
          this.movable_locations = this.subject_animal.getAvailableMoves();
          this.highlightMovableCells(this.movable_locations, true);
          this.state = States.ANIMAL_SELECTED;
        }
        break;

      case ANIMAL_SELECTED:
        Location new_selected_location = new Location(GridPane.getRowIndex((Pane) event.getSource()),
            GridPane.getColumnIndex((Pane) event.getSource()));

        // try to map `new_selected_location` to internal key of `this.movable_locations`
        for (Location location: this.movable_locations.keySet())
          if (new_selected_location.equals(location))
            new_selected_location = location;

        // deselect subject animal
        if (this.subject_animal.getLocation().equals(new_selected_location))
          this.resetToIdle();

        else
        {
          if (new ArrayList<>(this.movable_locations.keySet()).contains(new_selected_location))
          {
            Move move = this.movable_locations.get(new_selected_location);
            this.getCell(this.subject_animal.getLocation()).getChildren().clear();
            this.highlightMovableCells(this.movable_locations, false);

            ArrayList<SimpleEntry<Location, Animal>> move_result = this.subject_animal.performMove(move);
            if (!move_result.isEmpty())
            {
              for (SimpleEntry<Location, Animal> dead_animal: move_result)
              {
                this.getCell(dead_animal.getKey()).getChildren().clear();
                this.setCell(dead_animal.getValue().getLocation(), dead_animal.getValue());
                this.getCell(dead_animal.getValue().getLocation()).getStyleClass().add("dead_location");
                --this.alive_animals;
              }
            }

            ArrayList<Animal> dead_animals = move_result.stream().map(e -> e.getValue()).collect(Collectors.toCollection(ArrayList::new));
            if (move_result.isEmpty() || !dead_animals.contains(this.subject_animal))
              this.setCell(this.subject_animal.getLocation(), this.subject_animal);

            // reset game
            if (this.alive_animals == 1)
            {
              AnimalGUI.showAlert(AlertType.INFORMATION, "Game ends", this.actual_forest.getFinalStatus());
              this.actual_forest.init();
              this.state = States.IDLE;
              this.alive_animals = this.total_animals;
              for (Node node : this.grid_forest.getChildren())
              {
                if (node instanceof Pane)
                {
                  Pane cell_pane = (Pane) node; 
                  cell_pane.getChildren().clear();
                  cell_pane.getStyleClass().clear();
                  cell_pane.getStyleClass().add("grid_forest_cell");
                  if (GridPane.getColumnIndex(cell_pane) == 0) cell_pane.getStyleClass().add("first-column");
                  if (GridPane.getRowIndex(cell_pane) == 0) cell_pane.getStyleClass().add("first-row");
                }
              }

              for (Animal animal: this.actual_forest.getAnimals())
                this.setCell(animal.getLocation(), animal);

              return;
            }
            // end reset game

            this.resetToIdle();
          }
          else
          {
            // select another subject animal
            Animal new_selected_animal = this.actual_forest.getAnimalAt(new_selected_location);
            if (new_selected_animal != null && !new_selected_animal.equals(this.subject_animal) && new_selected_animal.isAlive())
            {
              this.subject_animal = this.actual_forest.getAnimalAt(new_selected_location);
              this.highlightMovableCells(this.movable_locations, false);
              this.movable_locations = this.subject_animal.getAvailableMoves();
              this.highlightMovableCells(this.movable_locations, true);
            }
          }
        }
        break;
      default:
        break;
    }
  };

  private EventHandler<MouseEvent> handleHoverCell = event -> {
    Location target_location = null;
    Location hovered_location = new Location(GridPane.getRowIndex((Pane) event.getSource()), GridPane.getColumnIndex((Pane) event.getSource()));
    if (this.state == States.ANIMAL_SELECTED)
    {
      target_location = (new ArrayList<>(this.movable_locations.keySet()).contains(hovered_location) ? hovered_location : null);
      if (target_location != null)
      {
        Pane cell = this.getCell(target_location);
        ObservableList<String> cell_style_classes = cell.getStyleClass();
        if (event.getEventType() == MouseEvent.MOUSE_ENTERED)
        {
          this.cached_class = cell_style_classes.get(cell_style_classes.size() - 1);
          cell_style_classes.remove(this.cached_class);
          cell_style_classes.add("hovered_location");
        }
        else if (event.getEventType() == MouseEvent.MOUSE_EXITED)
        {
          cell_style_classes.remove("hovered_location");
          cell_style_classes.add(this.cached_class);
          this.cached_class = null;
        }
      }
    }
  };
}
