package application;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.Parent;

/**
 * The application main class
 * @author andyyks
 */
public class AnimalGUI extends Application
{
  private static Stage pStage;
  public static final int NUM_COLS = 15;
  public static final int NUM_ROWS = 15;
  public static final int ICON_HEIGHT = 40;
  public static final int ICON_WIDTH = 40;

  @Override
  public void start(Stage stage)
  {
    AnimalGUI.pStage = stage;
    try
    {
      Parent root = FXMLLoader.load(getClass().getResource("/resources/StartupMenu.fxml"));
      Scene scene = new Scene(root);
      scene.getStylesheets().add("/resources/style.css");
      stage.setTitle("Startup Menu");
      stage.setResizable(false);
      stage.setScene(scene);
      stage.centerOnScreen();
      stage.show();
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
  }

  /**
   * Get the current application stage
   * @return The current application stage
   */
  public static Stage getStage()
  {
    return pStage;
  }

  /**
   * Show alert to user
   * @param alert_type
   * @param title
   * @param content_text
   */
  public static void showAlert(AlertType alert_type, String title, String content_text)
  {
    Alert alert = new Alert(alert_type);
    alert.setTitle(title);
    alert.setHeaderText(null);
    alert.setContentText(content_text);
    alert.showAndWait();
  }

  public static void main(String[] args)
  {
    launch(args);
  }
}