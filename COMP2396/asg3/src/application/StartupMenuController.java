package application;

import animals.*;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.Pane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javax.activation.MimetypesFileTypeMap;

/**
 * Control the Startup Menu
 * @author andyyks
 */
public class StartupMenuController
{
  private LinkedHashMap<CheckBox, Animal> map_checkboxes_animals;
  private LinkedHashMap<Button, Animal>   map_btn_icons_animals;
  private LinkedHashMap<CheckBox, Button> map_checkboxes_btn_icons;
  private LinkedHashMap<Animal, Pane>     map_animals_pane_icons;
  private ArrayList<Animal>               selected_animals = new ArrayList<Animal>();

  private String default_icon_dir = System.getProperty("user.dir") + "/";

  private Cat    cat    = new Cat(default_icon_dir + "icons/Cat.png");
  private Dog    dog    = new Dog(default_icon_dir + "icons/Dog.png");
  private Fox    fox    = new Fox(default_icon_dir + "icons/Fox.png");
  private Hippo  hippo  = new Hippo(default_icon_dir + "icons/Hippo.png");
  private Lion   lion   = new Lion(default_icon_dir + "icons/Lion.png");
  private Tiger  tiger  = new Tiger(default_icon_dir + "icons/Tiger.png");
  private Turtle turtle = new Turtle(default_icon_dir + "icons/Turtle.png");
  private Wolf   wolf   = new Wolf(default_icon_dir + "icons/Wolf.png");

  @FXML
  private CheckBox cb_cat, cb_dog, cb_fox, cb_hippo, cb_lion, cb_tiger, cb_turtle, cb_wolf;
  @FXML
  private Button btn_icon_cat, btn_icon_dog, btn_icon_fox, btn_icon_hippo, btn_icon_lion, btn_icon_tiger, btn_icon_turtle, btn_icon_wolf;
  @FXML
  private Pane pane_icon_cat, pane_icon_dog, pane_icon_fox, pane_icon_hippo, pane_icon_lion, pane_icon_tiger, pane_icon_turtle, pane_icon_wolf;
  @FXML
  private Button btn_start;

  @SuppressWarnings("serial")
  @FXML
  private void initialize()
  {
    this.map_checkboxes_btn_icons = new LinkedHashMap<CheckBox, Button>()
    {
      {
        put(cb_cat, btn_icon_cat);
        put(cb_dog, btn_icon_dog);
        put(cb_fox, btn_icon_fox);
        put(cb_hippo, btn_icon_hippo);
        put(cb_lion, btn_icon_lion);
        put(cb_tiger, btn_icon_tiger);
        put(cb_turtle, btn_icon_turtle);
        put(cb_wolf, btn_icon_wolf);
      }
    };
    this.map_checkboxes_animals = new LinkedHashMap<CheckBox, Animal>()
    {
      {
        put(cb_cat, cat);
        put(cb_dog, dog);
        put(cb_fox, fox);
        put(cb_hippo, hippo);
        put(cb_lion, lion);
        put(cb_tiger, tiger);
        put(cb_turtle, turtle);
        put(cb_wolf, wolf);
      }
    };
    this.map_btn_icons_animals = new LinkedHashMap<Button, Animal>()
    {
      {
        put(btn_icon_cat, cat);
        put(btn_icon_dog, dog);
        put(btn_icon_fox, fox);
        put(btn_icon_hippo, hippo);
        put(btn_icon_lion, lion);
        put(btn_icon_tiger, tiger);
        put(btn_icon_turtle, turtle);
        put(btn_icon_wolf, wolf);
      }
    };
    this.map_animals_pane_icons = new LinkedHashMap<Animal, Pane>()
    {
      {
        put(cat, pane_icon_cat);
        put(dog, pane_icon_dog);
        put(fox, pane_icon_fox);
        put(hippo, pane_icon_hippo);
        put(lion, pane_icon_lion);
        put(tiger, pane_icon_tiger);
        put(turtle, pane_icon_turtle);
        put(wolf, pane_icon_wolf);
      }
    };
    for (Map.Entry<Animal, Pane> entry : this.map_animals_pane_icons.entrySet())
      this.set_display_icon(entry.getKey());
  }

  @FXML
  private void inspect_btn_icon(ActionEvent event)
  {
    this.map_checkboxes_btn_icons.get(event.getSource()).setDisable(!((CheckBox)event.getSource()).isSelected());
  }

  @FXML
  private void select_alt_icon(ActionEvent event)
  {
    Animal target_animal = this.map_btn_icons_animals.get((Button)event.getSource());
    FileChooser fileChooser = new FileChooser();
    fileChooser.setTitle("Open Image File for " + target_animal.getClass().getSimpleName());
    fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Image Files", "*.gif", "*jpg", "*.jpeg", "*.png"));
    File file = fileChooser.showOpenDialog(null);
    MimetypesFileTypeMap mftp = new MimetypesFileTypeMap();
    mftp.addMimeTypes("image gif jpg jpeg png");
    if (file != null)
      if (mftp.getContentType(file).split("/")[0].equals("image"))
      {
        target_animal.setIcon(file.getPath());
        this.set_display_icon(target_animal);
      }
      else
        AnimalGUI.showAlert(AlertType.ERROR, "Error", "It's NOT an image");
  }

  @FXML
  private void start(ActionEvent event)
  {
    this.selected_animals.clear();

    for (Map.Entry<CheckBox, Animal> entry : this.map_checkboxes_animals.entrySet())
    {
      if (entry.getKey().isSelected())
      {
        Animal animal = entry.getValue();
        try
        {
          this.selected_animals.add(animal);
        }
        catch (Exception e)
        {
          // TODO Auto-generated catch block
          e.printStackTrace();
        }
      }
    }

    if (this.selected_animals.size() >= 2)
    {
      Forest actualForest = new Forest();
      for (Animal animal: this.selected_animals)
      {
        animal.setForest(actualForest);
        actualForest.regAnimal(animal);
      }

      try
      {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/resources/SimulatedForest.fxml"));
        loader.setController(new SimulatedForestController(actualForest, this.selected_animals.size()));
        Scene scene = new Scene(loader.load());
        scene.getStylesheets().add("/resources/style.css");
        Stage stage = AnimalGUI.getStage();
        stage.hide();
        stage.setTitle("Simulated Forest");
        stage.setScene(scene);
        stage.centerOnScreen();
        stage.show();
      }
      catch (IOException e)
      {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }
    }
    else
    {
      AnimalGUI.showAlert(AlertType.ERROR, "Error", "Must select at least 2 animals.");
    }
  }

  private void set_display_icon(Animal animal)
  {
    ObservableList<Node> children = this.map_animals_pane_icons.get(animal).getChildren();
    children.clear();
    children.add(animal.getIcon());
  }
}
