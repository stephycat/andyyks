package application;

import java.util.ArrayList;
import animals.*;
import libraries.*;

/**
 * The forest in memory for interacting with GUI
 * @author andyyks
 */
public class Forest
{
  private ArrayList<Animal> animals;
  private Animal board[][];

  /**
   * Creates a forest with animals at random locations
   */
  public Forest()
  {
    this.animals = new ArrayList<Animal>();
  }

  /**
   * Assign each animal to a randomized location
   */
  public void init()
  {
    ArrayList<Location> locations = new ArrayList<Location>();
    this.board = new Animal[AnimalGUI.NUM_ROWS][AnimalGUI.NUM_COLS];

    for (int y=0; y<AnimalGUI.NUM_ROWS; y++)
      for (int x=0; x<AnimalGUI.NUM_COLS; x++)
        locations.add(new Location(y, x));
 
    for (Animal animal: animals)
    {
      animal.revive();
      this.setAnimalAt(animal, animal.setLocation(Randomizer.draw(locations)));
    }
  }

  /**
   * Register an animal into the forest
   * @param animal
   */
  public void regAnimal(Animal animal)
  {
    this.animals.add(animal);
  }

  /**
   * Check whether a location is empty
   * @param location Target location
   * @return True for empty, False for being occupied by an animal
   */
  public boolean isEmpty(final Location location)
  {
    return (this.getAnimalAt(location) == null);
  }

  /**
   * Check whether a location is inside the forest
   * @param location Target location
   * @return True for inside the forest, False for outside the forest
   */
  public boolean isInside(final Location location)
  {
    int y = location.get(VComponent.Y);
    int x = location.get(VComponent.X);
    return (0 <= y && y < AnimalGUI.NUM_ROWS && 0 <= x && x < AnimalGUI.NUM_COLS);
  }

  /**
   * Get all animals currently in the forest
   * @return A set of animals
   */
  public ArrayList<Animal> getAnimals()
  {
    return this.animals;
  }

  /**
   * Get the animal occupied in a specific location
   * @param location Target location
   * @return Target animal
   */
  public Animal getAnimalAt(Location location)
  {
    int y = location.get(VComponent.Y);
    int x = location.get(VComponent.X);
    return this.board[y][x];
  }

  /**
   * Get alive/death information 
   * @return Alive/death information 
   */
  public String getFinalStatus()
  {
    String result = "";
    for (Animal animal: animals)
    result += (
      animal.getClass().getSimpleName() + " is " +
      (animal.isAlive() ? "alive" : "dead") + " at " + 
      animal.getLocation() + "\n"
    );
    return result;
  }

  /**
   * Set the animal to be occupied in a specific location
   * @param animal Target animal
   * @param location Target location
   */
  public void setAnimalAt(Animal animal, Location location)
  {
    int y = location.get(VComponent.Y);
    int x = location.get(VComponent.X);
    this.board[y][x] = animal;
  }
}