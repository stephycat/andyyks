package animals;

/**
 * Super class for foxes in the forest
 * @author andyyks
 */
public class Fox extends Canine
{
  /**
   * Creates a fox with specified forest
   * @param icon_path
   */
  public Fox(String icon_path)
  {
    super(icon_path);
  }

  @Override
  public boolean attack(Animal animal)
  {
    if (animal instanceof Cat)
      return animal.die();
    else
      return super.attack(animal);
  }
}