package animals;

/**
 * Super class for lions in the forest
 * @author andyyks
 */
public class Lion extends Feline
{
  /**
   * Creates a lion with specified forest
   * @param icon_path
   */
  public Lion(String icon_path)
  {
    super(icon_path);
  }

  @Override
  public boolean attack(Animal animal)
  {
    if (animal instanceof Hippo)
      return animal.die();
    else
      return super.attack(animal);
  }
}