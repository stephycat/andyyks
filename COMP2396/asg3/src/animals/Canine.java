package animals;

import static java.util.Arrays.asList;
//import javafx.scene.image.ImageView;
import libraries.*;

/**
 * Super class for canines in the forest
 * @author andyyks
 */
public abstract class Canine extends Animal
{
  /**
   * Creates a canine with label and specified forest 
   * @param icon_path
   */
  public Canine(String icon_path)
  {
    super(icon_path);
    this.defaultMoves.addAll(
      asList(
        new Move(Direction.NORTH, 2),
        new Move(Direction.EAST, 2),
        new Move(Direction.SOUTH, 2),
        new Move(Direction.WEST, 2)
      )
    );
  }

  @Override
  public boolean attack(Animal animal)
  {
    if (animal instanceof Feline)
      if (!animal.die(0.5))
        return !this.die();
      else
        return true;
    else
      return super.attack(animal);
  }
}