package animals;

/**
 * Super class for wolves in the forest
 * @author andyyks
 */
public class Wolf extends Canine
{
  /**
   * Creates a wolf with specified forest
   * @param icon_path
   */
  public Wolf(String icon_path)
  {
    super(icon_path);
  }
}