package animals;

/**
 * Super class for turtles in the forest
 * @author andyyks
 */
public class Turtle extends Animal
{
  /**
   * Creates a turtle with specified forest
   * @param icon_path
   */
  public Turtle(String icon_path)
  {
    super(icon_path);
  }

  @Override
  public boolean attack(Animal animal)
  {
    if (!animal.die(0.5))
      return !this.die();
    else
      return true;
  }
}