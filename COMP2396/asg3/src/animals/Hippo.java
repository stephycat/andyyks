package animals;

/**
 * Super class for hippos in the forest
 * @author andyyks
 */
public class Hippo extends Animal
{
  /**
   * Creates a hippo with specified forest
   * @param icon_path
   */
  public Hippo(String icon_path)
  {
    super(icon_path);
  }
}