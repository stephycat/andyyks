package animals;

/**
 * Super class for tigers in the forest
 * @author andyyks
 */
public class Tiger extends Feline
{
  /**
   * Creates a tiger with specified forest
   * @param icon_path
   */
  public Tiger(String icon_path)
  {
    super(icon_path);
  }
}