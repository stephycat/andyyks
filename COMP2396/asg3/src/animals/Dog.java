package animals;

/**
 * Super class for dogs in the forest
 * @author andyyks
 */
public class Dog extends Canine
{
  /**
   * Creates a dog with specified forest
   * @param icon_path
   */
  public Dog(String icon_path)
  {
    super(icon_path);
  }
}