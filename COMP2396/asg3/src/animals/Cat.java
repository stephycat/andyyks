package animals;

/**
 * Super class for cats in the forest
 * @author andyyks
 */
public class Cat extends Feline
{
  /**
   * Creates a cat with specified forest
   * @param icon_path
   */
  public Cat(String icon_path)
  {
    super(icon_path);
  }
}