package animals;

import static java.util.Arrays.asList;
//import javafx.scene.image.ImageView;
import libraries.*;

/**
 * Super class for felines in the forest
 * @author andyyks
 */
public abstract class Feline extends Animal
{
  /**
   * Creates a feline with label and specified forest 
   * @param icon_path
   */
  public Feline(String icon_path)
  {
    super(icon_path);
    this.defaultMoves.addAll(
      asList(
        new Move(Direction.NORTH_EAST, 1),
        new Move(Direction.SOUTH_EAST, 1),
        new Move(Direction.SOUTH_WEST, 1),
        new Move(Direction.NORTH_WEST, 1)
      )
    );
  }

  @Override
  public boolean attack(Animal animal)
  {
    if (animal instanceof Canine)
      return animal.die();
    else
      return super.attack(animal);
  }
}